LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := /home/ryan/DevLibrary/SDL2

LOCAL_C_INCLUDES := $(SDL_PATH)/include
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson-android/jni/sdl2_includes
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson-android/jni/sdl2image_includes
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/ext/TmxParser
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/ext/TmxParser/base64
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/ext/tinyxml
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/framework
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/gui
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/gui/rocket
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/map
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/render
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/resource
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/system
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson/include/util
LOCAL_C_INCLUDES += /home/ryan/DevLibrary/librocket-git/Include
LOCAL_C_INCLUDES += /home/ryan/Projects/crimson-android/jni/libtcod/include

# Add your application source files here...
LOCAL_SRC_FILES := os/android/SDL_android_main.cpp os/linux/main.cpp
LOCAL_SRC_FILES += framework/World.cpp
LOCAL_SRC_FILES += framework/ComponentManager.cpp
LOCAL_SRC_FILES += framework/EntityManager.cpp
LOCAL_SRC_FILES += framework/GameSystem.cpp
LOCAL_SRC_FILES += system/RenderSystem.cpp
LOCAL_SRC_FILES += system/PlayerSystem.cpp
LOCAL_SRC_FILES += system/GUIRenderSystem.cpp
LOCAL_SRC_FILES += system/PositionSystem.cpp
LOCAL_SRC_FILES += system/CameraSystem.cpp
LOCAL_SRC_FILES += system/AnimationSystem.cpp
LOCAL_SRC_FILES += component/AnimationComponent.cpp
LOCAL_SRC_FILES += component/BarrierComponent.cpp
LOCAL_SRC_FILES += component/CameraComponent.cpp
LOCAL_SRC_FILES += component/DescriptionComponent.cpp
LOCAL_SRC_FILES += component/GUIComponent.cpp
LOCAL_SRC_FILES += component/MapPositionComponent.cpp
LOCAL_SRC_FILES += component/PlayerComponent.cpp
LOCAL_SRC_FILES += component/PositionComponent.cpp
LOCAL_SRC_FILES += component/SpriteComponent.cpp
LOCAL_SRC_FILES += component/VelocityComponent.cpp
LOCAL_SRC_FILES += resource/ResourceManager.cpp
LOCAL_SRC_FILES += util/bitset.cpp
LOCAL_SRC_FILES += util/EventMessage.cpp
LOCAL_SRC_FILES += render/Renderer.cpp
LOCAL_SRC_FILES += map/TileEngine.cpp
LOCAL_SRC_FILES += gui/HUD.cpp
LOCAL_SRC_FILES += gui/HUDManager.cpp
LOCAL_SRC_FILES += gui/MainHUD.cpp
LOCAL_SRC_FILES += gui/rocket/RocketGlue.cpp
#LOCAL_SRC_FILES += gui/RocketSDLInterfaceOpenGLES.cpp
LOCAL_SRC_FILES += gui/rocket/RocketSDLInterfaceOpenGLES2.cpp
LOCAL_SRC_FILES += gui/rocket/ConsoleBuffer.cpp
LOCAL_SRC_FILES += gui/rocket/ConsoleMessageFormatter.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxImage.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxLayer.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxMap.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxObject.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxObjectGroup.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxPolygon.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxPolyline.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxPropertySet.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxTile.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxTileset.cpp
LOCAL_SRC_FILES += ext/TmxParser/TmxUtil.cpp
LOCAL_SRC_FILES += ext/TmxParser/base64/base64.cpp
LOCAL_SRC_FILES += ext/tinyxml/tinystr.cpp
LOCAL_SRC_FILES += ext/tinyxml/tinyxml.cpp
LOCAL_SRC_FILES += ext/tinyxml/tinyxmlerror.cpp
LOCAL_SRC_FILES += ext/tinyxml/tinyxmlparser.cpp

LOCAL_STATIC_LIBRARIES := tcod
LOCAL_STATIC_LIBRARIES += ROCKET
LOCAL_SHARED_LIBRARIES := SDL2
LOCAL_SHARED_LIBRARIES += SDL2_image

##$(call import-module freetype)

#lGLESv2 lGLESv1_CM
LOCAL_LDLIBS := -lz -lGLESv2 -llog

include $(BUILD_SHARED_LIBRARY)
