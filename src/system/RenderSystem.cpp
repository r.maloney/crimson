#include "Entity.h"
#include "Component.h"
#include "Space.h"
#include "RenderSystem.h"

renderSystem::renderSystem(Renderer renderer) : renderer(renderer) {
}

renderSystem::~renderSystem() {
}

void renderSystem::Process(Space space, Uint32 interval) {
    TileEngine tileEngine = space->tileEngine;

    tileEngine->RenderBackground(renderer);

    tileEngine->RenderGrid(renderer);

    Entity entity = this->entities.Head();
    while (NULL != entity) {
        SpriteComponent spriteComp = space->componentManager.GetComponentForEntity<spriteComponent>(entity);
        PhysicsComponent posComponent = space->componentManager.GetComponentForEntity<physicsComponent>(entity);

        spriteMetadata* sm = &(spriteComp->spriteMap[spriteComp->activeState]);
        b2Vec2 vec2 = posComponent->body->GetPosition();
        SDL_Point modifiedPosition;
        modifiedPosition.x = vec2.x;
        modifiedPosition.y = vec2.y;

        //The sprites upper left corner will be drawn from this modified position. Offset by half the sprite width and height
        //in either direction to draw the sprite such that it is centered on the world position for collision purposes.
        modifiedPosition.x -= sm->width/2;
        modifiedPosition.y -= sm->height/2;

        //Sprite consist of different, optional layers. Draw them in order. Each should be drawn to the same position
        //as the original even if each has a unique source rect.

        //(1) Draw the body
        renderer->Render(
                    sm->textureResource->textureRes->texture,
                    sm->sourceRect,
                    modifiedPosition);

        //(2) draw hair
        sm = &(spriteComp->headMap[spriteComp->activeState]);
        if (NULL != sm->textureResource) {
            renderer->Render(
                        sm->textureResource->textureRes->texture,
                        sm->sourceRect,
                        modifiedPosition);
        }

        //(3) draw torso
        sm = &(spriteComp->torsoMap[spriteComp->activeState]);
        if (NULL != sm->textureResource) {
            renderer->Render(
                        sm->textureResource->textureRes->texture,
                        sm->sourceRect,
                        modifiedPosition);
        }

        //(4) draw legs
        sm = &(spriteComp->legsMap[spriteComp->activeState]);
        if (NULL != sm->textureResource) {
            renderer->Render(
                        sm->textureResource->textureRes->texture,
                        sm->sourceRect,
                        modifiedPosition);
        }

        //(5) draw feet
        sm = &(spriteComp->feetMap[spriteComp->activeState]);
        if (NULL != sm->textureResource) {
            renderer->Render(
                        sm->textureResource->textureRes->texture,
                        sm->sourceRect,
                        modifiedPosition);
        }

        //(6) draw weapon
        sm = &(spriteComp->weaponMap[spriteComp->activeState]);
        if (NULL != sm->textureResource) {
            //Weapons can be oversized.
            modifiedPosition.x = vec2.x;
            modifiedPosition.y = vec2.y;
            modifiedPosition.x -= sm->width/2;
            modifiedPosition.y -= sm->height/2;
            renderer->Render(
                        sm->textureResource->textureRes->texture,
                        sm->sourceRect,
                        modifiedPosition);
        }

        entity = this->entities.Next(entity);
    }

    tileEngine->RenderForeground(renderer);
}

void renderSystem::AddEntity(Entity entity) {
    this->entities.InsertTail(entity);
}

void renderSystem::RemoveEntity(Entity entity) {
    entity->renderSystemLink.Unlink();
}
