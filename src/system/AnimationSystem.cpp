#include "Entity.h"
#include "Component.h"
#include "Space.h"
#include "AnimationSystem.h"

animationSystem::animationSystem() : intervalAccumulator(0) {
}

animationSystem::~animationSystem() {
}

void animationSystem::Process(Space space, Uint32 interval) {
    Entity entity = this->entities.Head();

    while (NULL != entity) {
        SpriteComponent spriteComp = space->componentManager.GetComponentForEntity<spriteComponent>(entity);
        AnimationComponent animComponent = space->componentManager.GetComponentForEntity<animationComponent>(entity);
        StatusComponent statComponent = space->componentManager.GetComponentForEntity<statusComponent>(entity);

        animationMetadata* am = &(animComponent->animationMap[statComponent->state]);

        am->intervalAccumulator += interval;

        //EARLY RETURN, process at desired framerate (typically 30 fps)
        if (am->intervalAccumulator < am->interval) {
            return;
        } else {
            am->intervalAccumulator = 0;
        }

        SDL_Point offset = am->animations[am->currentAnimationPointer];
        spriteComp->activeState = statComponent->state;
        spriteMetadata* sm = &(spriteComp->spriteMap[statComponent->state]);
        SDL_Rect newSourceRect;
        newSourceRect.x = offset.x * sm->width;
        int orientationOffset = offset.y;
        switch (statComponent->heading) {
        case WEST:
            orientationOffset += am->westFacingOffset.y;
            break;
        case EAST:
            orientationOffset += am->eastFacingOffset.y;
            break;
        case NORTH:
            orientationOffset += am->northFacingOffset.y;
            break;
        case SOUTH:
            orientationOffset += am->southFacingOffset.y;
            break;
        }

        newSourceRect.y = ((offset.y + orientationOffset) * sm->height);

        //Update BODY sprite metadata
        sm->sourceRect.x = newSourceRect.x;
        sm->sourceRect.y = newSourceRect.y;

        //Update HEAD sprite metadata
        sm = &(spriteComp->headMap[statComponent->state]);
        sm->sourceRect.x = newSourceRect.x;
        sm->sourceRect.y = newSourceRect.y;

        //Update TORSO sprite metadata
        sm = &(spriteComp->torsoMap[statComponent->state]);
        sm->sourceRect.x = newSourceRect.x;
        sm->sourceRect.y = newSourceRect.y;

        //Update LEGS sprite metadata
        sm = &(spriteComp->legsMap[statComponent->state]);
        sm->sourceRect.x = newSourceRect.x;
        sm->sourceRect.y = newSourceRect.y;

        //Update FEET sprite metadata
        sm = &(spriteComp->feetMap[statComponent->state]);
        sm->sourceRect.x = newSourceRect.x;
        sm->sourceRect.y = newSourceRect.y;

        //Update WEAPON sprite metadata
        sm = &(spriteComp->weaponMap[statComponent->state]);
        //Weapons can be oversized
        newSourceRect.x = (offset.x * sm->width);
        newSourceRect.y = ((offset.y + orientationOffset) * sm->height);
        sm->sourceRect.x = newSourceRect.x;
        sm->sourceRect.y = newSourceRect.y;

        am->currentAnimationPointer++;

        if (am->currentAnimationPointer >= am->animations.size()) {
            am->currentAnimationPointer = 0;
            //Attack lasts as long as it takes to animate.
            if (statComponent->state == ATTACK || statComponent->state == HIT) {
                statComponent->state = IDLE;
            }
        }

        entity = this->entities.Next(entity);
    }
}

void animationSystem::AddEntity(Entity entity) {
    this->entities.InsertTail(entity);
}

void animationSystem::RemoveEntity(Entity entity) {
    entity->animationSystemLink.Unlink();
}
