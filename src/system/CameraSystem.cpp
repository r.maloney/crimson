#include <stdlib.h>

#include "Entity.h"
#include "Component.h"
#include "Space.h"
#include "CameraSystem.h"
#include "TileEngine.h"

cameraSystem::cameraSystem(Renderer renderer) : renderer(renderer) {
}

cameraSystem::~cameraSystem() {
}

void cameraSystem::Process(Space space, Uint32 interval) {
    TileEngine tileEngine = space->tileEngine;

    Entity entity = this->entities.Head();
    while (NULL != entity) {
        CameraComponent camComponent = space->componentManager.GetComponentForEntity<cameraComponent>(entity);
        PhysicsComponent posComponent = space->componentManager.GetComponentForEntity<physicsComponent>(entity);

        b2Vec2 position = posComponent->body->GetPosition();
        camComponent->SetLookAt(position.x, position.y);

        //The following blocks makes the camera chase the player but stop once it hits the edge of a map.
        SDL_Rect const* mapBoundaries = tileEngine->GetMapBoundaries();

        int cameraFarX = camComponent->viewBounds.x + camComponent->viewBounds.w;
        int mapFarX = mapBoundaries->x + mapBoundaries->w;

        if (camComponent->viewBounds.x < mapBoundaries->x) {
            camComponent->lookAt.x += abs(mapBoundaries->x - camComponent->viewBounds.x);
            camComponent->viewBounds.x = mapBoundaries->x;
        } else if ( cameraFarX > mapFarX ) {
            int delta = cameraFarX - mapFarX;
            camComponent->lookAt.x -= delta;
            camComponent->viewBounds.x -= delta;
        }

        int cameraFarY = camComponent->viewBounds.y + camComponent->viewBounds.h;
        int mapFarY = mapBoundaries->y + mapBoundaries->h;

        if (camComponent->viewBounds.y < mapBoundaries->y) {
            camComponent->lookAt.y += abs(mapBoundaries->y - camComponent->viewBounds.y);
            camComponent->viewBounds.y = mapBoundaries->y;
        } else if ( cameraFarY > mapFarY ) {
            int delta = cameraFarY - mapFarY;
            camComponent->lookAt.y -= delta;
            camComponent->viewBounds.y -= delta;
        }

        renderer->view = camComponent->viewBounds;

        entity = this->entities.Next(entity);
    }
}

void cameraSystem::AddEntity(Entity entity) {
    this->entities.InsertTail(entity);
}

void cameraSystem::RemoveEntity(Entity entity) {
    entity->cameraSystemLink.Unlink();
}
