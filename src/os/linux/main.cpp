#if defined(__ANDROID__)
#else
#include <logog/logog.hpp>
#include <logog/target.hpp>
#endif

#include <SDL2/SDL_version.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <Rocket/Core/Context.h>
#include <Rocket/Debugger.h>

#include "RocketGlue.h"
#include "Renderer.h"
#include "EntityManager.h"
#include "ComponentManager.h"
#include "Entity.h"
#include "Component.h"
#include "ResourceManager.h"
#include "Logger.h"
#include "TileEngine.h"
#include "EventManager.h"
#include "EventInstancer.h"
#include "GameElement.h"
#include "Space.h"

int main(int argc, char *argv[]) {
#if defined(__ANDROID__)
#else
    LOGOG_INITIALIZE();
    {
        logog::Cout out;
#endif

        SDL_version compiled;
        SDL_version linked;

        SDL_VERSION(&compiled);
        SDL_GetVersion(&linked);
        LOGI("We compiled against SDL version %d.%d.%d ...\n",
             compiled.major, compiled.minor, compiled.patch);
        LOGI("But we are linking against SDL version %d.%d.%d.\n",
             linked.major, linked.minor, linked.patch);

        SDL_version sdlImageCompileVersion;
        const SDL_version* sdlImageLinkVersion = IMG_Linked_Version();

        SDL_IMAGE_VERSION(&sdlImageCompileVersion);
        LOGI("Compiled with SDL_image version: %d.%d.%d\n",
             sdlImageCompileVersion.major,
             sdlImageCompileVersion.minor,
             sdlImageCompileVersion.patch);
        LOGI("Running with SDL_image version: %d.%d.%d\n",
             sdlImageLinkVersion->major,
             sdlImageLinkVersion->minor,
             sdlImageLinkVersion->patch);

        if( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO | SDL_INIT_JOYSTICK)< 0) {
            LOGE("Unable to initialize SDL: %s\n", SDL_GetError());
            assert(0);
        }

        //TODO: Replace with intelligent resolution detection
#if defined(__ANDROID__)
        int ResolutionWidth = 1024;
        int ResolutionHeight = 600;
#else
        int ResolutionWidth = 800;
        int ResolutionHeight = 600;
#endif

        SDL_Window* window = NULL;

        window = SDL_CreateWindow(
                    "crimson",
                    SDL_WINDOWPOS_CENTERED,
                    SDL_WINDOWPOS_CENTERED,
                    ResolutionWidth,
                    ResolutionHeight,
                    SDL_WINDOW_OPENGL
                    );

        if(NULL == window){
            LOGE("Error creating SDL screen: %s\n", SDL_GetError());
            assert(0);
        }

        Renderer renderer = CreateRenderer(window);

        SDL_RendererInfo renderInfo;
        SDL_GetRendererInfo(renderer->sdlRenderer, &renderInfo);

        if(0 == strcmp("opengl", renderInfo.name)) {
            LOGI("%s", "SDL Renderer is using OpenGL");
        } else if (0 == strcmp("opengles", renderInfo.name)) {
            LOGI("%s","SDL Renderer is using OpenGLES");
        } else if (0 == strcmp("opengles2", renderInfo.name)) {
            LOGI("%s","SDL Renderer is using OpenGLES2");
        } else {
            assert(0);
        }

        //Initialize Resource manager
#if defined(__ANDROID__)
        ResourceManager resourceManager= CreateResourceManager(renderer->sdlRenderer, "");
#else
        ResourceManager resourceManager= CreateResourceManager(renderer->sdlRenderer, "assets/");
#endif

        AddResource(resourceManager, "character/walk/male_walkcycle.png", TEXTURE);
        AddResource(resourceManager, "character/walk/FEET_shoes_brown.png", TEXTURE);
        AddResource(resourceManager, "character/walk/LEGS_pants_greenish.png", TEXTURE);
        AddResource(resourceManager, "character/walk/TORSO_leather_armor_shirt_white.png", TEXTURE);
        AddResource(resourceManager, "character/walk/HEAD_hair_blonde.png", TEXTURE);
        AddResource(resourceManager, "character/slash/male_slash.png", TEXTURE);
        AddResource(resourceManager, "character/slash/FEET_shoes_brown.png", TEXTURE);
        AddResource(resourceManager, "character/slash/LEGS_pants_greenish.png", TEXTURE);
        AddResource(resourceManager, "character/slash/TORSO_leather_armor_shirt_white.png", TEXTURE);
        AddResource(resourceManager, "character/slash/HEAD_hair_blonde.png", TEXTURE);
        AddResource(resourceManager, "character/slash/WEAPON_dagger.png", TEXTURE);
        AddResource(resourceManager, "character/slash/WEAPON_longsword.png", TEXTURE);
        AddResource(resourceManager, "character/immobile/BODY_animation.png", TEXTURE);
        AddResource(resourceManager, "ground/LPC/base_out_atlas.png", TEXTURE);
        AddResource(resourceManager, "ground/LPC/terrain_atlas.png", TEXTURE);
        AddResource(resourceManager, "cursor/cursor.png", TEXTURE);
        AddResource(resourceManager, "entity/Player.entity", ENTITY);
        AddResource(resourceManager, "entity/CombatDummy.entity", ENTITY);
        AddResource(resourceManager, "map/City.tmx", TMX_MAP);
        AddResource(resourceManager, "map/TestMap.tmx", TMX_MAP);

        //Test
        space spaceTest(resourceManager, renderer);

        //Enable world and start loop
        spaceTest.processing = true;

        //Start up Rocket
        Rocket::Core::Context* context = RocketInit(renderer->sdlRenderer, window);
        if (NULL == context) {
            LOGE("%s", "An error occurred while attempting to generate a rocket context.");
            assert(0);
        }

        //        Rocket::Debugger::Initialise(context);
        //        Rocket::Debugger::SetVisible(true);

        Rocket::Core::ElementInstancer* element_instancer = new Rocket::Core::ElementInstancerGeneric< GameElement >();
        Rocket::Core::Factory::RegisterElementInstancer("game", element_instancer);
        element_instancer->RemoveReference();

        EventManager::context = context;
        EventInstancer* eventInstancer = new EventInstancer();
        Rocket::Core::Factory::RegisterEventListenerInstancer(eventInstancer);
        eventInstancer->RemoveReference();

        if (!EventManager::LoadWindow("game")) {
            assert(0);
        }
        GameElement* gameElement = static_cast<GameElement*>(Rocket::Core::ElementUtilities::GetElementById(context->GetFocusElement(), "game"));
        gameElement->SetSpace(&spaceTest);
        gameElement->Focus();

        //Track FPS
        int numFrames = 0;
        Uint32 startTime = SDL_GetTicks();
        Uint32 lastTime = SDL_GetTicks();
        Uint32 now = 0;
        float fps = 0.f;
        Rocket::Core::Element* fpsElement = context->GetDocument("game_window")->GetElementById("fps");
        if (NULL == fpsElement) {
            assert(0);
        }

        static SDL_Event keyEvent;

        while (spaceTest.processing) {
            now = SDL_GetTicks();
            renderer->Clear(0, 0, 0, 255);

            while (SDL_PollEvent(&keyEvent)) {
                InjectRocket(context, keyEvent);
            }
            context->Update();

            spaceTest.Process(now - lastTime);

            context->Render();

            spaceTest.RenderDebugCollisionData();

            renderer->Flip();

            lastTime = now;
            if (0 == ++numFrames % 100) {
                fps = ( numFrames/(float)(now - startTime) )*1000;
                LOGI("%f FPS.",fps);
                fpsElement->SetInnerRML(Rocket::Core::String(128, "%d", (int)fps).CString());
            }
        }

        FreeResourceManager(resourceManager);
        FreeRenderer(renderer);

        SDL_DestroyWindow(window);
        SDL_Quit();
#if defined(__ANDROID__)
#else
    }

    LOGOG_SHUTDOWN();
#endif

    return EXIT_SUCCESS;
}
