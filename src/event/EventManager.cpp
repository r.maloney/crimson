#include "EventManager.h"
#include <Rocket/Core/ElementDocument.h>
#include <Rocket/Core/ElementUtilities.h>
#include "EventHandler.h"
#include <map>

Rocket::Core::Context* EventManager::context;

// The event handler for the current screen. This may be NULL if the current screen has no specific functionality.
static EventHandler* event_handler = NULL;

// The event handlers registered with the manager.
typedef std::map< Rocket::Core::String, EventHandler* > EventHandlerMap;
EventHandlerMap event_handlers;

EventManager::EventManager(){
}

EventManager::~EventManager(){
}

void EventManager::Shutdown() {
    for (EventHandlerMap::iterator i = event_handlers.begin(); i != event_handlers.end(); ++i) {
        delete (*i).second;
    }

    event_handlers.clear();
    event_handler = NULL;
}

void EventManager::RegisterEventHandler(const Rocket::Core::String& handler_name, EventHandler* handler) {
    EventHandlerMap::iterator iterator = event_handlers.find(handler_name);
    if (iterator != event_handlers.end()) {
        delete (*iterator).second;
    }

    event_handlers[handler_name] = handler;
}

void EventManager::ProcessEvent(Rocket::Core::Event& event, const Rocket::Core::String& value) {
    Rocket::Core::StringList commands;
    Rocket::Core::StringUtilities::ExpandString(commands, value, ';');
    for (size_t i = 0; i < commands.size(); ++i) {
        Rocket::Core::StringList values;
        Rocket::Core::StringUtilities::ExpandString(values, commands[i], ' ');

        if (values.empty()) {
            return;
        }

        if (values[0] == "goto" && values.size() > 1) {
            if (LoadWindow(values[1])) {
                event.GetTargetElement()->GetOwnerDocument()->Close();
            }
        } else if (values[0] == "load" && values.size() > 1) {
            LoadWindow(values[1]);
        } else if (values[0] == "close") {
            Rocket::Core::ElementDocument* target_document = NULL;

            if (values.size() > 1) {
                target_document = context->GetDocument(values[1].CString());
            } else {
                target_document = event.GetTargetElement()->GetOwnerDocument();
            }

            if (target_document != NULL) {
                target_document->Close();
            }
        } else if (values[0] == "exit") {
            //Shell::RequestExit();
        } else if (values[0] == "pause") {
            //GameDetails::SetPaused(true);
        } else if (values[0] == "unpause") {
            //GameDetails::SetPaused(false);
        } else {
            if (event_handler != NULL) {
                event_handler->ProcessEvent(event, commands[i]);
            }
        }
    }
}

bool EventManager::LoadWindow(const Rocket::Core::String& window_name) {
    EventHandler* old_event_handler = event_handler;
    EventHandlerMap::iterator iterator = event_handlers.find(window_name);
    if (iterator != event_handlers.end()) {
        event_handler = (*iterator).second;
    } else {
        event_handler = NULL;
    }

    Rocket::Core::String document_path = window_name + Rocket::Core::String(".rml");
    Rocket::Core::ElementDocument* document = context->LoadDocument(document_path.CString());
    if (document == NULL) {
        event_handler = old_event_handler;
        return false;
    }

    Rocket::Core::Element* title = document->GetElementById("title");
    if (title != NULL) {
        title->SetInnerRML(document->GetTitle());
    }

    document->Focus();
    document->Show();

    document->RemoveReference();

    return true;
}
