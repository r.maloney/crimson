#include "Renderer.h"
#include "Logger.h"

#include <cassert>
#if SDL_VIDEO_RENDER_OGL_ES
#include <SDL2/SDL_opengles.h>

void renderer::Render(SDL_Texture* texture, SDL_Rect& sourceRect, SDL_Rect& destRect, SDL_Point& worldCoords) {
      //Translate sprite's world coordinates to screen coordinates and check if it is within
    //the screen.
//    if (GetScreenCoordinates(destRect, worldCoords)) {
//        SDL_RenderCopy(this->sdlRenderer,
//                       texture,
//                       &sourceRect,
//                       &destRect);
//    }
}

void renderer::Clear(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
  glClearColor( 255.0f, 0.0f, 0.0f, 0.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void renderer::Flip() {
    SDL_RenderPresent(this->sdlRenderer);
}

//Loads the destRect with the screen coords if inside screen. Returns
//true if worldCoords are in screen, false otherwise
bool renderer::GetScreenCoordinates(SDL_Rect& destRect, SDL_Point& worldCoords) {
    bool contains = false;

    int screenX = worldCoords.x - this->view.x;
    int screenY = worldCoords.y - this->view.y;

    bool xRange, yRange = false;
    if (screenX >= 0 && screenX < (view.w - destRect.w)) {
        xRange = true;
    }
    if (screenY >= 0 && screenY < (view.h - destRect.h)) {
        yRange = true;
    }

    if (xRange && yRange) {
        destRect.x = screenX;
        destRect.y = screenY;
        contains = true;
    }

    return contains;
}

Renderer CreateRenderer(SDL_Window* window) {
    SDL_Renderer* sdlRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_TARGETTEXTURE | SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if(NULL == sdlRenderer){
        LOGE("Error creating SDL renderer: %s\n", SDL_GetError());
        assert(0);
    }

    Renderer obj = new renderer();
    obj->sdlRenderer = sdlRenderer;
    SDL_RenderGetViewport(sdlRenderer, &obj->view);

   /* Enable smooth shading */
    glShadeModel( GL_SMOOTH );
 
    /* Set the background black */
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
 
    /* Depth buffer setup */
    glClearDepth( 1.0f );
 
    /* Enables Depth Testing */
    glEnable( GL_DEPTH_TEST );
 
    /* The Type Of Depth Test To Do */
    glDepthFunc( GL_LEQUAL );
 
    /* Really Nice Perspective Calculations */
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );

    return obj;
}


void FreeRenderer(Renderer renderer) {
    SDL_DestroyRenderer(renderer->sdlRenderer);
    delete renderer;
}
#endif
