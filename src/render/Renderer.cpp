#include "Renderer.h"
#include "Logger.h"

#include <cassert>

void renderer::Render(SDL_Texture* texture, SDL_Rect& sourceRect, SDL_Point& worldCoords) {
    //Translate sprite's world coordinates to screen coordinates, check if it is within
    //the screen and clip the sprite's source and destination rectangle to fit in the view.
    SDL_Rect clippedDestRect;
    SDL_Rect clippedSourceRect = sourceRect;
    if (GetScreenCoordinates(&clippedSourceRect, clippedDestRect, worldCoords)) {
        SDL_RenderCopy(this->sdlRenderer,
                       texture,
                       &clippedSourceRect,
                       &clippedDestRect);
    }
}

void renderer::Render(SDL_Color* color, SDL_Rect& destRect, SDL_Point& worldCoords) {
    //Translate sprite's world coordinates to screen coordinates and check if it is within
    //the screen.
    SDL_Rect clippedSourceRect = destRect;
    SDL_SetRenderDrawColor(this->sdlRenderer, color->r, color->g, color->b, 255);
    if (GetScreenCoordinates(&clippedSourceRect, destRect, worldCoords)) {
        SDL_RenderDrawRect(this->sdlRenderer, &destRect);
    }
}

void renderer::RenderLine(SDL_Color& color, SDL_Point& p1WorldCoords, SDL_Point& p2WorldCoords) {
    SDL_SetRenderDrawColor(this->sdlRenderer, color.r, color.g, color.b, 255);
    SDL_RenderDrawLine(sdlRenderer,
                       p1WorldCoords.x - this->view.x,
                       p1WorldCoords.y - this->view.y,
                       p2WorldCoords.x - this->view.x,
                       p2WorldCoords.y - this->view.y );
}

void renderer::RenderPoint(SDL_Color& color, SDL_Point& worldCoords) {
    SDL_RenderDrawPoint(sdlRenderer,
                       worldCoords.x - this->view.x,
                       worldCoords.y - this->view.y);
}

void renderer::RenderCircle(SDL_Color& color, SDL_Point& center, int radius) {
    //From wikipedia: http://en.wikipedia.org/wiki/Midpoint_circle_algorithm
    //Not perfect but good enough.
    SDL_SetRenderDrawColor(this->sdlRenderer, color.r, color.g, color.b, 255);
    int x = radius;
    int y = 0;
    int xChange = 1 - radius*2;
    int yChange = 0;
    int radiusError = 0;

    int x0 = center.x - this->view.x;
    int y0 = center.y - this->view.y;

    while(x >= y) {
        SDL_RenderDrawPoint(sdlRenderer, x + x0, y+y0);
        SDL_RenderDrawPoint(sdlRenderer, x + x0, y + y0);
        SDL_RenderDrawPoint(sdlRenderer, y + x0, x + y0);
        SDL_RenderDrawPoint(sdlRenderer, -x + x0, y + y0);
        SDL_RenderDrawPoint(sdlRenderer, -y + x0, x + y0);
        SDL_RenderDrawPoint(sdlRenderer, -x + x0, -y + y0);
        SDL_RenderDrawPoint(sdlRenderer, -y + x0, -x + y0);
        SDL_RenderDrawPoint(sdlRenderer, x + x0, -y + y0);
        SDL_RenderDrawPoint(sdlRenderer, y + x0, -x + y0);

        y++;
        radiusError += yChange;
        yChange += 2;
        if(((radiusError << 1) + xChange) > 0) {
            x--;
            radiusError += xChange;
            xChange += 2;
        }
    }
}

void renderer::Clear(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
    SDL_SetRenderDrawColor(this->sdlRenderer, r, g, b, a);
    SDL_RenderClear(this->sdlRenderer);
}

void renderer::Flip() {
    SDL_RenderPresent(this->sdlRenderer);
}

bool renderer::GetScreenCoordinates(SDL_Rect* sourceRect, SDL_Rect& destRect, SDL_Point& worldCoords) {
    bool contains = false;

    SDL_Rect hitRect;
    hitRect.x = worldCoords.x - this->view.x;
    hitRect.y = worldCoords.y - this->view.y;
    hitRect.h = sourceRect->h;
    hitRect.w = sourceRect->w;

    SDL_Rect cameraRect;
    cameraRect.x = 0;
    cameraRect.y = 0;
    cameraRect.w = this->view.w;
    cameraRect.h = this->view.h;

    if (SDL_TRUE == SDL_IntersectRect(
                &cameraRect,
                &hitRect,
                &destRect)) {

        contains = true;

        //It is possible for the rectangles to intersect with the camera but hang over the edge. This
        //is acceptable, consider it a normal hit.
        destRect = hitRect;
    }

    return contains;
}

SDL_Point renderer::ConvertScreenCoordsToWorld(SDL_Point& screenCoords, SDL_Rect& destRect) {
    SDL_Point worldCoords;
    worldCoords.x = (((this->view.x + (screenCoords.x) ) / destRect.w) * destRect.w);
    worldCoords.y = (((this->view.y + (screenCoords.y) ) / destRect.h) * destRect.h);

    return worldCoords;
}

Renderer CreateRenderer(SDL_Window* window) {
    SDL_Renderer* sdlRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_TARGETTEXTURE | SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if(NULL == sdlRenderer){
        LOGE("Error creating SDL renderer: %s\n", SDL_GetError());
        assert(0);
    }

    Renderer obj = new renderer();
    obj->sdlRenderer = sdlRenderer;
    SDL_RenderGetViewport(sdlRenderer, &obj->view);

    return obj;
}

void FreeRenderer(Renderer renderer) {
    SDL_DestroyRenderer(renderer->sdlRenderer);
    delete renderer;
}
