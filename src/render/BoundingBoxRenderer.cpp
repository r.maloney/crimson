#include "BoundingBoxRenderer.h"

boundingBoxRenderer::boundingBoxRenderer(Renderer renderer)
    : b2Draw(),
      renderer(renderer){

}

void boundingBoxRenderer::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {
    for (unsigned int i = 0; i < vertexCount - 1; ++i) {
        this->DrawSegment(vertices[i], vertices[i+1], color);
    }
    this->DrawSegment(vertices[0], vertices[vertexCount - 1], color);
}

void boundingBoxRenderer::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {
    this->DrawPolygon(vertices, vertexCount, color);
}

void boundingBoxRenderer::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color) {
   SDL_Color sdlColor;
   sdlColor.r = color.r;
   sdlColor.g = color.g;
   sdlColor.b = color.b;

   SDL_Point sdlCenter;
   sdlCenter.x = center.x;
   sdlCenter.y = center.y;

   this->renderer->RenderCircle(sdlColor, sdlCenter, radius);
}

void boundingBoxRenderer::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color) {
    this->DrawCircle(center, radius, color);
}

void boundingBoxRenderer::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) {
    SDL_Color sdlColor;
    sdlColor.r = color.r;
    sdlColor.g = color.g;
    sdlColor.b = color.b;
    SDL_Point point1;
    point1.x = p1.x;
    point1.y = p1.y;
    SDL_Point point2;
    point2.x = p2.x;
    point2.y = p2.y;

    this->renderer->RenderLine(sdlColor, point1, point2);
}

void boundingBoxRenderer::DrawTransform(const b2Transform& xf) {

}
