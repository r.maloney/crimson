#include <map>
#include <vector>

#include "Entity.h"
#include "Logger.h"

#include "TmxMap.h"
#include "TmxTile.h"
#include "TmxTileset.h"
#include "TileEngine.h"
#include "TmxImage.h"
#include "TmxLayer.h"
#include "TmxMapTile.h"
#include "TmxObject.h"
#include "TmxObjectGroup.h"
#include "TmxPropertySet.h"

#define COLLISION_DATA ((internalCollisionData *)this->collisionData)
#define ENGINE_DATA ((internalTileEngineData *)this->data)

TileEngine CreateTileEngine(ResourceManager resourceManager,
                            unsigned int mapWidth,
                            unsigned int mapHeight,
                            unsigned int tileWidth,
                            unsigned int tileHeight,
                            unsigned int layers) {
    ResourceHandle cursorTexture = GetResource(resourceManager, "cursor/cursor.png", TEXTURE);
    return new tileEngine(mapWidth, mapHeight, tileWidth, tileHeight, layers, cursorTexture);
}

//http://gamedev.tutsplus.com/tutorials/implementation/parsing-tiled-tmx-format-maps-in-your-own-game-engine/
TileEngine CreateTileEngine(ResourceManager resourceManager,
                            b2World* box2dWorld,
                            std::string tmxFileName) {
    ResourceHandle tmxResourceHandle = GetResource(resourceManager, tmxFileName, TMX_MAP);
    Tmx::Map* tmxMap = &(tmxResourceHandle->tmxMapResource->map);
    LOGI("Number of tilesets in map %s: %d", tmxFileName.c_str(), tmxMap->GetNumTilesets());
    LOGI("Number of layers in map %s: %d", tmxFileName.c_str(), tmxMap->GetNumLayers());
    LOGI("Dimensions of map %s: (%d x %d)", tmxFileName.c_str(), tmxMap->GetWidth(), tmxMap->GetHeight());

    ResourceHandle cursorTexture = GetResource(resourceManager, "cursor/cursor.png", TEXTURE);

    TileEngine newTileEngine = new tileEngine(tmxMap->GetWidth(),
                                              tmxMap->GetHeight(),
                                              tmxMap->GetTileWidth(),
                                              tmxMap->GetTileHeight(),
                                              tmxMap->GetNumLayers(),
                                              cursorTexture,
                                              box2dWorld);

    std::vector<Tmx::Tileset*> tileSets = tmxMap->GetTilesets();

    for (std::vector<Tmx::Tileset*>::iterator it = tileSets.begin() ; it != tileSets.end(); ++it) {
        Tmx::Tileset* tileSet = *(it);

        LOGI("Tileset source: %s", tileSet->GetImage()->GetSource().c_str());

        int numTilesWide = tileSet->GetImage()->GetWidth() / tileSet->GetTileWidth();
        int numTilesHigh = tileSet->GetImage()->GetHeight() / tileSet->GetTileHeight();

        int tileWidthCounter = 0;
        int tileHeightCounter = 0;

        for (unsigned int i = (unsigned int) tileSet->GetFirstGid(); i <= (tileSet->GetFirstGid() + numTilesWide * numTilesHigh); ++i) {
            unsigned int xOffsetPixels = tileWidthCounter * tileSet->GetTileWidth();
            unsigned int yOffsetPixels = tileHeightCounter * tileSet->GetTileHeight();

            //TODO: I'm not sure why this is the case, dig in and find out
            int gid = i -1;

            newTileEngine->RegisterTile(resourceManager,
                                        gid,
                                        tileSet->GetImage()->GetSource(),
                                        xOffsetPixels,
                                        yOffsetPixels);

            if (++tileWidthCounter == numTilesWide) {
                tileWidthCounter = 0;
                tileHeightCounter++;
            }
        }
    }

    for (unsigned int layer = 0; layer < tmxMap->GetNumLayers(); layer++) {
        for (unsigned int x = 0; x < tmxMap->GetWidth(); x++) {
            for (unsigned int y = 0; y < tmxMap->GetHeight(); y++){

                Tmx::MapTile mapTile = tmxMap->GetLayer(layer)->GetTile(x, y);
                newTileEngine->SetTile(mapTile.id, x, y, layer);

                if (0 != mapTile.id) {
                    const Tmx::Tile* tile = tmxMap->GetTileset(mapTile.tilesetId)->GetTile(mapTile.id);

                    //Default to true
                    bool isTraversable = true;
                    bool isTransparent = true;

                    if (NULL != tile) {
                        std::string traversable = tile->GetProperties().GetLiteralProperty("traversable");
                        std::string transparent = tile->GetProperties().GetLiteralProperty("transparent");

                        if (traversable.length() > 0) {
                            if (0 == traversable.compare("false")) {
                                isTraversable = false;
                            }
                        }
                        if (transparent.length() > 0) {
                            if (0 == transparent.compare("false")) {
                                isTransparent = false;
                            }
                        }
                    }

                    newTileEngine->UpdateTerrain(x, y, isTraversable, isTransparent);
                }
            }
        }
    }

    for (unsigned int i = 0; i < tmxMap->GetNumObjectGroups(); ++i) {
        const Tmx::ObjectGroup* objectGroup = tmxMap->GetObjectGroup(i);
        for (unsigned int oCount = 0; oCount < objectGroup->GetNumObjects(); oCount++) {
            const Tmx::Object* object = objectGroup->GetObject(oCount);

            SDL_Rect objData;
            objData.x = object->GetX();
            objData.y = object->GetY();
            objData.w = object->GetWidth();
            objData.h = object->GetHeight();

            newTileEngine->AddObject(objData);
        }
    }

    return newTileEngine;
}

typedef struct terrainData {
    bool traversable;
    bool transparent;
} internalTerrainData;

typedef struct objectData {
    SDL_Rect boundingBox;
} internalObjectData;

typedef struct entityData {
    Entity entity;
} internalEntityData;

typedef struct tileData {
    SDL_Rect sourceRect;
    ResourceHandle resourceHandle;
} internalTileData;

typedef struct collisionData {
    std::vector< std::vector<terrainData> > terrain;
    std::vector<objectData> object;
    std::vector< std::vector<entityData> > entity;
} internalCollisionData;

typedef struct tileEngineData {
    int mapWidth,mapHeight;
    int tileWidth,tileHeight;
    int layers;
    SDL_Rect mapBoundaries;
    std::vector< std::vector< std::vector<int> > > tileMap;
    std::map<unsigned int, internalTileData> tileData;
    ResourceHandle cursorTexture;
} internalTileEngineData;

tileEngine::tileEngine(unsigned int mapWidth,
                       unsigned int mapHeight,
                       unsigned int tileWidth,
                       unsigned int tileHeight,
                       unsigned int layers,
                       ResourceHandle cursorTexture,
                       b2World* box2dWorld)
    : drawGrid(false),
      box2dWorld(box2dWorld){

    internalTileEngineData* tileEngineData = new internalTileEngineData();
    tileEngineData->mapWidth = mapWidth;
    tileEngineData->mapHeight = mapHeight;
    tileEngineData->tileWidth = tileWidth;
    tileEngineData->tileHeight = tileHeight;
    tileEngineData->mapBoundaries.x = 0;
    tileEngineData->mapBoundaries.y = 0;
    tileEngineData->mapBoundaries.w = mapWidth * tileWidth;
    tileEngineData->mapBoundaries.h = mapHeight * tileHeight;
    tileEngineData->layers = layers;
    tileEngineData->cursorTexture = cursorTexture;

    //First allocate enough room for all layers.
    tileEngineData->tileMap.resize(layers);

    for (int l = 0; l < layers; ++l)  {
        //Second allocate enough room for all rows for each layer
        tileEngineData->tileMap[l].resize(mapHeight);
        for (int y = 0; y < mapHeight; ++y) {
            //Third allocate enough room for all columns for each row in each layer
            tileEngineData->tileMap[l][y].resize(mapWidth);
            for (int x = 0; x < mapWidth; ++x) {
                //finally initialize
                tileEngineData->tileMap[l][y][x] = 0;
            }
        }
    }

    this->data = (void*)tileEngineData;

    internalCollisionData* collisionData = new internalCollisionData();

    collisionData->terrain.resize(mapHeight);
    collisionData->entity.resize(mapHeight);

    for (int y = 0; y < mapHeight; ++y) {
        collisionData->terrain[y].resize(mapWidth);
        collisionData->entity[y].resize(mapWidth);
    }

    this->collisionData = (void*)collisionData;
}

tileEngine::~tileEngine() {
    delete ENGINE_DATA;
    delete COLLISION_DATA;
}

void tileEngine::RegisterTile(ResourceManager resourceManager,
                              unsigned int id,
                              std::string filename,
                              int x,
                              int y) {

    unsigned int mapSize = ENGINE_DATA->tileData.size();

    internalTileData* internalTileData = &ENGINE_DATA->tileData[id];

    //Default behavior is for the map to create an entry if the id being searched on
    //doesn't exist thereby increasing the size of the map by 1. In this scenario initialize
    //the tile metadata otherwise leave the present value.
    if (mapSize < ENGINE_DATA->tileData.size()) {
        internalTileData->sourceRect.x = x;
        internalTileData->sourceRect.y = y;
        internalTileData->sourceRect.w = ENGINE_DATA->tileWidth;
        internalTileData->sourceRect.h = ENGINE_DATA->tileHeight;
        internalTileData->resourceHandle = GetResource(resourceManager, filename, TEXTURE);
    }
}

void tileEngine::SetTile(unsigned int id,
                         int x,
                         int y,
                         int layer) {
    ENGINE_DATA->tileMap[layer][x][y] = id;
    internalTileData* internalTileData = &ENGINE_DATA->tileData[id];
}

unsigned int tileEngine::GetTile(int x, int y, int layer){
    return ENGINE_DATA->tileMap[layer][x][y];
}

void tileEngine::UpdateTerrain(int x,
                               int y,
                               bool traversable /* = true*/,
                               bool transparent /* = true*/) {
    COLLISION_DATA->terrain[x][y].transparent = transparent;
    COLLISION_DATA->terrain[x][y].transparent = traversable;
}

void tileEngine::AddObject(SDL_Rect objectBoundingBox) {
    internalObjectData objData;
    objData.boundingBox = objectBoundingBox;
    COLLISION_DATA->object.push_back(objData);

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    //The upper left of the bounding box of the object needs to be the CENTER
    //of the physics body.
    bodyDef.position.Set(objectBoundingBox.x + objectBoundingBox.w/2, objectBoundingBox.y + objectBoundingBox.h/2);

    b2PolygonShape boundingBox;
    boundingBox.SetAsBox(objectBoundingBox.w/2, objectBoundingBox.h/2);

    b2FixtureDef collisionFixtureDef;
    collisionFixtureDef.shape = &boundingBox;

    b2Body* staticBody = box2dWorld->CreateBody(&bodyDef);
    staticBody->CreateFixture(&collisionFixtureDef);
}

bool tileEngine::IsTraversable(SDL_Rect player) {
    std::vector<internalObjectData>::iterator it = (COLLISION_DATA->object).begin();
    static SDL_Rect result;

    for (; it != COLLISION_DATA->object.end(); ++it) {
        if (SDL_IntersectRect(&(it->boundingBox), &player, &result)) {
            return false;
        }
    }

    return true;
}

void tileEngine::RenderBackground(Renderer renderer){
    static SDL_Point worldCoords;

    int xMin, yMin, xMax, yMax = 0;
    CalculateVisibleMap(renderer, xMin, xMax, yMin, yMax);

    //Top most layer is foreground
    for(int layer=0; layer < ENGINE_DATA->layers - 1; layer++) {
        for (int y=yMin; y < yMax ; y++) {
            for (int x = xMin; x < xMax; x++ ) {
                unsigned int tileId = ENGINE_DATA->tileMap[layer][x][y];
                worldCoords.x = (x * ENGINE_DATA->tileWidth);
                worldCoords.y = (y * ENGINE_DATA->tileHeight);

                //Don't draw non existent tiles
                if (0 != tileId) {
                    internalTileData* tile = &ENGINE_DATA->tileData[tileId];

                    SDL_SetTextureColorMod(tile->resourceHandle->textureRes->texture, 255, 255, 255);

                    renderer->Render(
                                tile->resourceHandle->textureRes->texture,
                                tile->sourceRect,
                                worldCoords);
                }
            }
        }
    }
}

void tileEngine::RenderForeground(Renderer renderer){
    static SDL_Point worldCoords;

    int xMin, yMin, xMax, yMax = 0;
    CalculateVisibleMap(renderer, xMin, xMax, yMin, yMax);

    for (int y=yMin;y < yMax; y++) {
        for (int x=xMin; x < xMax; x++ ) {

            unsigned int tileId = ENGINE_DATA->tileMap[ENGINE_DATA->layers-1][x][y];
            worldCoords.x = (x * ENGINE_DATA->tileWidth);
            worldCoords.y = (y * ENGINE_DATA->tileHeight);

            //Don't draw non existent tiles
            if (0 != tileId) {
                internalTileData* tile = &ENGINE_DATA->tileData[tileId];

                SDL_SetTextureColorMod(tile->resourceHandle->textureRes->texture, 255, 255, 255);

                renderer->Render(
                            tile->resourceHandle->textureRes->texture,
                            tile->sourceRect,
                            worldCoords);
            }
        }
    }
}

void tileEngine::RenderGrid(Renderer renderer){
    if (this->drawGrid) {
        static SDL_Rect gridSource;
        gridSource.x = 0;
        gridSource.y = 0;
        gridSource.w = 32;
        gridSource.h = 32;

        static SDL_Rect cursorSource;
        cursorSource.x = 32;
        cursorSource.y = 0;
        cursorSource.w = 32;
        cursorSource.h = 32;

        static SDL_Color gridColor;
        gridColor.r = 0;
        gridColor.g = 0;
        gridColor.b = 0;

        static SDL_Color highlightColor;
        highlightColor.r = 255;
        highlightColor.g = 255;
        highlightColor.b = 0;

        static SDL_Point worldCoords;

        static SDL_Rect gridDestRect;
        gridDestRect.x = 0;
        gridDestRect.y = 0;
        gridDestRect.w = ENGINE_DATA->tileWidth;
        gridDestRect.h = ENGINE_DATA->tileHeight;

        //Render highlighted tile under mouse if the mouse world
        //coords fall within the map coordinates.
        static SDL_Rect mouseHighlightRect;
        mouseHighlightRect.w = ENGINE_DATA->tileWidth;
        mouseHighlightRect.h = ENGINE_DATA->tileHeight;

        static SDL_Point points[1];

        int xMin, yMin, xMax, yMax = 0;
        CalculateVisibleMap(renderer, xMin, xMax, yMin, yMax);

        for (int y = yMin;y < yMax; y++) {
            for (int x = xMin; x < xMax; x++ ) {

                worldCoords.x = (x * ENGINE_DATA->tileWidth);
                worldCoords.y = (y * ENGINE_DATA->tileHeight);

                renderer->Render(
                            ENGINE_DATA->cursorTexture->textureRes->texture,
                            gridSource,
                            worldCoords);
            }
        }

        points[0] = renderer->ConvertScreenCoordsToWorld(
                    mouseLocation,
                    mouseHighlightRect);

        //we don't need this so will be ignored, just prevents sending in NULL
        static SDL_Rect result;

        if (SDL_TRUE == SDL_EnclosePoints(points,
                                          1,
                                          &(ENGINE_DATA->mapBoundaries),
                                          &result)) {
            this->selectedTile.x = points[0].x / ENGINE_DATA->tileWidth;
            this->selectedTile.y = points[0].y / ENGINE_DATA->tileHeight;

            renderer->Render(
                        ENGINE_DATA->cursorTexture->textureRes->texture,
                        cursorSource,
                        points[0]);
        }
    }
}

void tileEngine::CalculateVisibleMap(Renderer renderer, int& xMin, int& xMax, int& yMin, int& yMax) {
    xMin = renderer->view.x/ENGINE_DATA->tileWidth;
    yMin = renderer->view.y/ENGINE_DATA->tileHeight;

    //Buffer a row for smooth scrolling
    xMax = xMin + (renderer->view.w/ENGINE_DATA->tileWidth) + 2;
    yMax = yMin + (renderer->view.h/ENGINE_DATA->tileHeight) + 2;

    xMax = (xMax > ENGINE_DATA->mapWidth) ? ENGINE_DATA->mapWidth : xMax;
    yMax = (yMax > ENGINE_DATA->mapHeight) ? ENGINE_DATA->mapHeight : yMax;

    xMin = (xMin >= 0) ? xMin : 0;
    yMin = (yMin >= 0) ? yMin : 0;

}

void tileEngine::ToggleGridDraw() {
    this->drawGrid = !this->drawGrid;
}

unsigned int tileEngine::GetTileWidth() {
    return ENGINE_DATA->tileWidth;
}

unsigned int tileEngine::GetTileHeight() {
    return ENGINE_DATA->tileHeight;
}

SDL_Rect const* tileEngine::GetMapBoundaries() {
    return &ENGINE_DATA->mapBoundaries;
}
