#include "GameElement.h"
#include "EventManager.h"
#include "Logger.h"

#include <math.h>
#include <Rocket/Core/ElementDocument.h>
#include <Rocket/Core/Input.h>
#include <SDL2/SDL.h>

#define XBOX_BUTTON_A 0

GameElement::GameElement(const Rocket::Core::String& tag)
    : Rocket::Core::Element(tag),
      amountToIncrementEachRound(0.0f),
      joystick(NULL){

    SDL_ShowCursor(SDL_ENABLE);

    if (SDL_NumJoysticks() > 0) {
        LOGI("%i joysticks were found.\n", SDL_NumJoysticks() );

        for( int i=0; i < SDL_NumJoysticks(); i++ )  {
            LOGI("Joystick %i name: %s\n", i, SDL_JoystickNameForIndex(i));
        }

        SDL_JoystickEventState(SDL_ENABLE);
        joystick = SDL_JoystickOpen(0);
    }
}

GameElement::~GameElement() {
}

void GameElement::ProcessEvent(Rocket::Core::Event& event) {
    Rocket::Core::Element::ProcessEvent(event);

    using namespace Rocket::Core::Input;

    if ( event == "keydown" || event == "keyup" ) {
        bool keyDown = (event == "keydown");
        Rocket::Core::Input::KeyIdentifier keyId = (Rocket::Core::Input::KeyIdentifier) event.GetParameter< int >("key_identifier", 0);

        if (keyDown) {
            switch (keyId) {
            case KI_Z:
                space->drawCollisionDebugData = !space->drawCollisionDebugData;
                break;
            case KI_TAB:
                tileEngine->ToggleGridDraw();
                break;
            case KI_ESCAPE:
                space->processing = false;
                break;
            case KI_SPACE:
                statusComp->state = ATTACK;
                physicsComp->body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));

                break;
            }
        } else {
            if (keyId != KI_SPACE
                    && keyId != KI_TAB
                    && keyId != KI_Z) {
                physicsComp->body->ApplyLinearImpulse( b2Vec2(0.0f,0.0f), physicsComp->body->GetWorldCenter() );
                statusComp->state = IDLE;
            }
        }
    } else if (event == "mousemove") {
        int mouseX = event.GetParameter<int>("mouse_x", 0);
        int mouseY = event.GetParameter<int>("mouse_y", 0);

        tileEngine->mouseLocation.x = mouseX;
        tileEngine->mouseLocation.y = mouseY;
    }

    if (event == "load") {
        //    game->Initialise();
    }
}

void GameElement::OnUpdate() {
    static Uint8 *keystate = SDL_GetKeyboardState(NULL);
    static float velocity = 35.0f;

    if (ATTACK != statusComp->state) {
        this->weaponBody->SetActive(false);
        b2Vec2 vel = physicsComp->body->GetLinearVelocity();
        float desiredVelX = 0.0f;
        float desiredVelY = 0.0f;

        if ( keystate[SDL_SCANCODE_UP] || keystate[SDL_SCANCODE_W]) {
            desiredVelX += 0.0f;
            desiredVelY += -velocity;
            statusComp->state = WALK;
            statusComp->heading = NORTH;
        }

        if ( keystate[SDL_SCANCODE_DOWN] || keystate[SDL_SCANCODE_S]) {
            desiredVelX += 0.0f;
            desiredVelY += velocity;
            statusComp->state = WALK;
            statusComp->heading = SOUTH;
        }

        if ( keystate[SDL_SCANCODE_LEFT] || keystate[SDL_SCANCODE_A]) {
            desiredVelX += -velocity;
            desiredVelY += 0.0f;
            statusComp->state = WALK;
            statusComp->heading = WEST;
        }

        if ( keystate[SDL_SCANCODE_RIGHT] || keystate[SDL_SCANCODE_D]) {
            desiredVelX += velocity;
            desiredVelY += 0.0f;
            statusComp->state = WALK;
            statusComp->heading = EAST;
        }

        if (NULL != joystick) {
            Sint16 xMove = (SDL_JoystickGetAxis(joystick, 0));
            bool xMoved = false;
            if (xMove < -10000) {
                desiredVelX += -velocity;
                desiredVelY += 0.0f;
                statusComp->state = WALK;
                statusComp->heading = WEST;
                xMoved = true;
            } else if (xMove > 10000) {
                desiredVelX += velocity;
                desiredVelY += 0.0f;
                statusComp->state = WALK;
                statusComp->heading = EAST;
                xMoved = true;
            }

            Sint16 yMove = (SDL_JoystickGetAxis(joystick, 1));
            bool yMoved = false;
            if (yMove < -10000) {
                desiredVelX += 0.0f;
                desiredVelY += -velocity;
                statusComp->state = WALK;
                statusComp->heading = NORTH;
                yMoved = true;
            } else if (yMove > 10000) {
                desiredVelX += 0.0f;
                desiredVelY += velocity;
                statusComp->state = WALK;
                statusComp->heading = SOUTH;
                yMoved = true;
            }

            if (!xMoved && !yMoved) {
                physicsComp->body->ApplyLinearImpulse( b2Vec2(0.0f,0.0f), physicsComp->body->GetWorldCenter() );
                statusComp->state = IDLE;
            }

            if (SDL_JoystickGetButton(joystick, XBOX_BUTTON_A)){
                statusComp->state = ATTACK;
                physicsComp->body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
            }
        }

        float velChangeX = desiredVelX - vel.x;
        float velChangeY = desiredVelY - vel.y;
        float impulseX = physicsComp->body->GetMass() * velChangeX; //disregard time factor
        float impulseY = physicsComp->body->GetMass() * velChangeY; //disregard time factor
        physicsComp->body->ApplyLinearImpulse( b2Vec2(impulseX,impulseY), physicsComp->body->GetWorldCenter() );
        this->weaponBody->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
        this->weaponBody->SetTransform(physicsComp->body->GetTransform().p, physicsComp->body->GetTransform().q.GetAngle());
    } else {
        this->weaponBody->SetActive(true);
        float rotation = 0.0f;
        b2Vec2 position = physicsComp->body->GetTransform().p;

        switch(this->statusComp->heading) {
        case WEST:
            rotation = -1.0f * (M_PI_4 - ( this->animationComp->animationMap[ATTACK].currentAnimationPointer * this->amountToIncrementEachRound));
            break;
        case EAST:
            rotation = M_PI_4 - ( this->animationComp->animationMap[ATTACK].currentAnimationPointer * this->amountToIncrementEachRound);
            break;
        case NORTH:
            rotation = M_PI_2 + ( this->animationComp->animationMap[ATTACK].currentAnimationPointer * this->amountToIncrementEachRound);
            position.y += 18.0f;
            break;
        case SOUTH:
            rotation = M_PI_2 - ( this->animationComp->animationMap[ATTACK].currentAnimationPointer * this->amountToIncrementEachRound);
            position.y -= 18.0f;
            break;
        }

        this->weaponBody->SetTransform(position, rotation);
    }
}

void GameElement::OnRender() {
}

void GameElement::OnChildAdd(Rocket::Core::Element* element) {
    Rocket::Core::Element::OnChildAdd(element);

    if (element == this) {
        GetOwnerDocument()->AddEventListener("load", this);
    }
}

void GameElement::SetSpace(Space space) {
    this->space = space;
    this->tileEngine = space->tileEngine;
    this->player = space->player;
    this->statusComp = space->componentManager.GetComponentForEntity<statusComponent>(this->player);
    this->physicsComp = space->componentManager.GetComponentForEntity<physicsComponent>(this->player);
    this->animationComp = space->componentManager.GetComponentForEntity<animationComponent>(this->player);

    //Again, a bit hacky for now until weapon equip is sorted out.
    b2BodyDef weaponBodyDef;
    weaponBodyDef.type = b2_dynamicBody;
    weaponBodyDef.position = this->physicsComp->body->GetPosition();
    weaponBodyDef.angle = 0.0f;
    weaponBodyDef.linearVelocity.Set(0.0f, 0.0f);

    b2Vec2 vertices[5];
    vertices[0].Set(-5, 60);
    vertices[1].Set(-5, 0);
    vertices[2].Set( 5, 0);
    vertices[3].Set( 5, 60);

    b2PolygonShape polygonShape;
    polygonShape.Set(vertices, 4); //pass array to the shape6
    b2FixtureDef weaponFixtureDef;
    weaponFixtureDef.shape = &polygonShape;
    weaponFixtureDef.filter.categoryBits = PLAYER_WEAPON;
    weaponFixtureDef.filter.maskBits = MOB;
    weaponFixtureDef.density = 1.0f;

    this->weaponBody = space->box2dWorld.CreateBody(&weaponBodyDef);
    this->weaponBody->CreateFixture(&weaponFixtureDef);
    this->weaponBody->SetUserData(new weapon());
    this->weaponBody->SetActive(true);

    this->amountToIncrementEachRound = (M_PI) / 6;
}
