#include "ConsoleBuffer.h"
#include <Rocket/Core/TypeConverter.h>
#include <cstdio>
#include <cassert>

ConsoleBuffer* ConsoleBuffer::instance = NULL;

ConsoleBuffer::ConsoleBuffer() : Rocket::Controls::DataSource("console_buffer") {
    ROCKET_ASSERT(instance == NULL);
    instance = this;

    systemMsgColor = Rocket::Core::Colourb(255, 255, 255);
    Rocket::Core::TypeConverter< Rocket::Core::Colourb, Rocket::Core::String >::Convert(systemMsgColor,systemMsgColorString);
    moveMsgColor = Rocket::Core::Colourb(0, 0, 255);
    Rocket::Core::TypeConverter< Rocket::Core::Colourb, Rocket::Core::String >::Convert(moveMsgColor,moveMsgColorString);
    attackMsgColor = Rocket::Core::Colourb(255, 0, 0);
    Rocket::Core::TypeConverter< Rocket::Core::Colourb, Rocket::Core::String >::Convert(attackMsgColor,attackMsgColorString);

    this->messages.resize(BUFFER_SIZE);
}

ConsoleBuffer::~ConsoleBuffer() {
    ROCKET_ASSERT(instance == this);
    instance = NULL;
}

void ConsoleBuffer::Initialise() {
    new ConsoleBuffer();
}

void ConsoleBuffer::Shutdown() {
    delete instance;
}

void ConsoleBuffer::GetRow(Rocket::Core::StringList& row, const Rocket::Core::String& table, int row_index, const Rocket::Core::StringList& columns) {
    if (table == "console" && row_index < messages.size()) {
        for (size_t i = 0; i < columns.size(); i++) {
            if (columns[i] == "message") {
                eventMessage* em = &(messages[row_index]);
                Rocket::Core::String msg(em->message.c_str());
                row.push_back(msg);
            } else if (columns[i] == "color") {
                eventMessage* em = &(messages[row_index]);
                switch (em->type) {
                case SYSTEM_MSG:
                    row.push_back(systemMsgColorString);
                    break;
                case MOVE_MSG:
                    row.push_back(moveMsgColorString);
                    break;
                case ATTACK_MSG:
                    row.push_back(attackMsgColorString);
                    break;
                default:
                    assert(0);
                }
            }
        }
    }
}

int ConsoleBuffer::GetNumRows(const Rocket::Core::String& table) {
    if (table == "console") {
        return (messages.size() < BUFFER_SIZE) ? messages.size() : BUFFER_SIZE;
    }

    return 0;
}

void ConsoleBuffer::AddMessage(eventMessage eventMessageParam) {
    instance->InternalAddMessage(eventMessageParam);
}

void ConsoleBuffer::InternalAddMessage(eventMessage eventMessageParam) {
    bool maxRows = false;

    if (messages.size() == BUFFER_SIZE) {
        messages.pop_front();
        maxRows = true;
    }

    messages.push_back(eventMessageParam);

    // Send the row removal message (if necessary).
    if (maxRows) {
        NotifyRowRemove("console", 0, 1);
    }

    // Then send the rows added message.
    NotifyRowAdd("console", messages.size() - 1, 1);

    return;
}
