#include "RocketSDLRenderInterfaceOpenGL.h"

#if SDL_VIDEO_RENDER_OGL
#include <SDL2/SDL_opengl.h>

RocketSDLRenderInterfaceOpenGL::RocketSDLRenderInterfaceOpenGL(SDL_Renderer *r, SDL_Window *w)
    : RocketSDLRenderInterface(r,w) {
}

void RocketSDLRenderInterfaceOpenGL::RenderGeometry(Rocket::Core::Vertex* vertices,
                                                    int num_vertices,
                                                    int* indices,
                                                    int num_indices,
                                                    const Rocket::Core::TextureHandle texture,
                                                    const Rocket::Core::Vector2f& translation) {
    glPushMatrix();
    glTranslatef(translation.x, translation.y, 0);

    std::vector<Rocket::Core::Vector2f> Positions(num_vertices);
    std::vector<Rocket::Core::Colourb> Colors(num_vertices);
    std::vector<Rocket::Core::Vector2f> TexCoords(num_vertices);
    float texw, texh;

    SDL_Texture* sdl_texture = NULL;
    if(texture) {
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        sdl_texture = (SDL_Texture *) texture;
        SDL_GL_BindTexture(sdl_texture, &texw, &texh);
    }

    for(int  i = 0; i < num_vertices; i++) {
        Positions[i] = vertices[i].position;
        Colors[i] = vertices[i].colour;
        if (sdl_texture) {
            TexCoords[i].x = vertices[i].tex_coord.x * texw;
            TexCoords[i].y = vertices[i].tex_coord.y * texh;
        }
        else {
            TexCoords[i] = vertices[i].tex_coord;
        }
    }

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, &Positions[0]);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, &Colors[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &TexCoords[0]);

    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_INT, indices);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);


    if (sdl_texture) {
        SDL_GL_UnbindTexture(sdl_texture);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    }

    glColor4f(1.0, 1.0, 1.0, 1.0);
    glPopMatrix();
}

void RocketSDLRenderInterfaceOpenGL::EnableScissorRegion(bool enable) {
    if (enable) {
        glEnable(GL_SCISSOR_TEST);
    } else {
        glDisable(GL_SCISSOR_TEST);
    }
}

void RocketSDLRenderInterfaceOpenGL::SetScissorRegion(int x, int y, int width, int height) {
    int w_width, w_height;
    SDL_GetWindowSize(window, &w_width, &w_height);
    glScissor(x, w_height - (y + height), width, height);
}
#endif
