#include "ConsoleMessageFormatter.h"
#include <Rocket/Core/TypeConverter.h>
#include <cassert>

ConsoleMessageFormatter::ConsoleMessageFormatter() : Rocket::Controls::DataFormatter("console_msg"){
}

ConsoleMessageFormatter::~ConsoleMessageFormatter(){
}

void ConsoleMessageFormatter::FormatData(Rocket::Core::String& formatted_data, const Rocket::Core::StringList& raw_data) {
    // Data format:
    // raw_data[0] is the colour, in "%d, %d, %d, %d" format.
    assert(2 == raw_data.size());
    formatted_data = "<div style=\"color: rgba(" + raw_data[1] + ");\">" + raw_data[0]+ " </div>";
}
