#include "ResourceManager.h"
#include "Logger.h"

#include <cassert>
#include <vector>
#include <map>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

typedef struct resourceManager {
    SDL_Renderer* renderer;
    std::map<std::string, ResourceHandle> resources;
    std::string assetBaseDir;
} internalResourceManager;

ResourceHandle CreateResourceHandle(ResourceType resourceType) {
    ResourceHandle rh = new resourceHandle();
    rh->resourceType = resourceType;

    switch (resourceType) {
    case TEXTURE:
        rh->textureRes = new textureResource();
        break;
    case TMX_MAP:
        rh->tmxMapResource = new tmxMapResource();
        break;
    case ENTITY:
        rh->entityTemplateResource = new entityTemplateResource();
        break;
    default:
        assert(0);
    }

    return rh;
}

void FreeResourceHandle(ResourceManager resourceManager, ResourceHandle resourceHandle) {
    switch (resourceHandle->resourceType) {
    case TEXTURE:
        resourceManager->resources[resourceHandle->textureRes->name] = NULL;
        SDL_DestroyTexture(resourceHandle->textureRes->texture);
        delete(resourceHandle->textureRes);
        break;
    case TMX_MAP:
        delete (resourceHandle->tmxMapResource);
        break;
    case ENTITY:
        delete (resourceHandle->entityTemplateResource);
        break;
    default:
        assert(0);
    }

    delete(resourceHandle);
}

ResourceManager CreateResourceManager(SDL_Renderer* renderer, std::string assetBaseDir) {
    ResourceManager obj = new internalResourceManager();
    obj->renderer = renderer;
    obj->assetBaseDir = assetBaseDir;

    return obj;
}

void FreeResourceManager(ResourceManager resourceManager) {
    std::map<std::string, ResourceHandle>::iterator itrBegin = resourceManager->resources.begin();
    std::map<std::string, ResourceHandle>::iterator itrEnd = resourceManager->resources.end();

    for (; itrBegin != itrEnd; ++itrBegin) {
        FreeResourceHandle(resourceManager, itrBegin->second);
    }
    delete(resourceManager);
}

void AddResource(ResourceManager resourceManager, std::string identifier, ResourceType resourceType) {
    ResourceHandle resourceHandle = CreateResourceHandle(resourceType);

    switch (resourceType) {
    case TEXTURE: {
        std::string strId =  resourceManager->assetBaseDir + identifier;
        resourceHandle->textureRes->texture = IMG_LoadTexture(resourceManager->renderer, strId.c_str());
        if(!resourceHandle->textureRes->texture) {
            LOGE("Unable to load resource: %s\n", strId.c_str());
            FreeResourceHandle(resourceManager, resourceHandle);
            assert(0);
        }
        resourceHandle->textureRes->name = identifier; }
        break;
    case TMX_MAP: {
        std::string strId = resourceManager->assetBaseDir + identifier;
        resourceHandle->tmxMapResource->map.ParseFile(strId);
        break;
    }
    case ENTITY: {
        std::string strId = resourceManager->assetBaseDir + identifier;
        SDL_RWops * file = SDL_RWFromFile (strId.c_str(), "rb");

        if (!file) {
            assert(0);
        }

        int fileSize = file->size(file);

        if (fileSize <= 0) {
            assert(0);
        }

        char* fileText = new char[fileSize + 1];
        fileText[fileSize] = 0;
        file->read(file, fileText, 1, fileSize);

        file->close(file);

        std::string text(fileText, fileText+fileSize);
        delete [] fileText;

        resourceHandle->entityTemplateResource->xml.Parse(text.c_str());

        break;
    }
    default:
        assert(0);
    }

    resourceManager->resources[identifier] = resourceHandle;
}

ResourceHandle GetResource(ResourceManager resourceManager, std::string identifier, ResourceType resourceType) {
    return resourceManager->resources[identifier];
}
