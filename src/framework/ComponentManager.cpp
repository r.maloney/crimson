#include <cassert>
#include <vector>

#include "ComponentManager.h"
#include "ComponentPool.h"
#include "bitset.h"

compManager::compManager(unsigned int startingPoolSize, unsigned int maximumEntityCount)
    : animationComponentPool(startingPoolSize),
      cameraComponentPool(startingPoolSize),
      physicsComponentPool(startingPoolSize),
      spriteComponentPool(startingPoolSize),
      statusComponentPool(startingPoolSize) {
    this->entityComponentMap.resize(maximumEntityCount * NUM_COMPONENT_TYPES);
}

compManager::~compManager() {
    this->entityComponentMap.clear();
}

AnimationComponent compManager::AddComponentToEntity(Entity entity, animationCompTag tag) {
    AnimationComponent comp = animationComponentPool.AllocateComponent();
    this->entityComponentMap[entity->id * NUM_COMPONENT_TYPES + tag.index] = comp;
    SetBit(entity->componentDescriptor, ANIMATION);

    return comp;
}

CameraComponent compManager::AddComponentToEntity(Entity entity, cameraCompTag tag) {
    CameraComponent comp = cameraComponentPool.AllocateComponent();
    this->entityComponentMap[entity->id * NUM_COMPONENT_TYPES + tag.index] = comp;
    SetBit(entity->componentDescriptor, CAMERA);

    return comp;
}

PhysicsComponent compManager::AddComponentToEntity(Entity entity, physicsCompTag tag) {
    PhysicsComponent comp = physicsComponentPool.AllocateComponent();
    this->entityComponentMap[entity->id * NUM_COMPONENT_TYPES + tag.index] = comp;
    SetBit(entity->componentDescriptor, PHYSICS);

    return comp;
}

SpriteComponent compManager::AddComponentToEntity(Entity entity, spriteCompTag tag) {
    SpriteComponent comp = spriteComponentPool.AllocateComponent();
    this->entityComponentMap[entity->id * NUM_COMPONENT_TYPES + tag.index] = comp;
    SetBit(entity->componentDescriptor, SPRITE);

    return comp;
}

StatusComponent compManager::AddComponentToEntity(Entity entity, statusCompTag tag) {
    StatusComponent comp = statusComponentPool.AllocateComponent();
    this->entityComponentMap[entity->id * NUM_COMPONENT_TYPES + tag.index] = comp;
    SetBit(entity->componentDescriptor, STATUS);

    return comp;
}
