#include <stdlib.h>
#include <assert.h>
#include <vector>

#include "EntityManager.h"

typedef struct entityManager {
    std::vector<Entity> entities;
    std::vector<int> reclaimedIds;
    unsigned int entityCount;
    unsigned int reclaimedIdCount;
} internalEntityManager;

typedef struct entity internalEntity;

unsigned int GetNextAvailableEntityId(EntityManager entityManager) {
    unsigned int reclaimedId = 0;

    if (entityManager->reclaimedIdCount > 0) {
        reclaimedId = entityManager->reclaimedIds[entityManager->reclaimedIdCount];
        entityManager->reclaimedIds[entityManager->reclaimedIdCount--] = 0;
    } else {
        reclaimedId = entityManager->entityCount++;
        assert(reclaimedId < MAX_ENTITY_NUM);
    }

    return reclaimedId;
}

EntityManager CreateEntityManager(void) {
    EntityManager obj = new internalEntityManager();
    obj->entities.resize(MAX_ENTITY_NUM);
    obj->reclaimedIds.resize(MAX_ENTITY_NUM);
    obj->entityCount = 0;
    obj->reclaimedIdCount = 0;

    return obj;
}

void FreeEntityManager(EntityManager entityManager) {
    for(unsigned int i = 0; i < entityManager->entityCount; ++i) {
        Entity entity = entityManager->entities[i];

        //Remove entity from all intrusive lists
        if (entity->renderSystemLink.IsLinked()) {
            entity->renderSystemLink.Unlink();
        }
        if (entity->cameraSystemLink.IsLinked()) {
            entity->cameraSystemLink.Unlink();
        }
        if (entity->animationSystemLink.IsLinked()) {
            entity->animationSystemLink.Unlink();
        }
        if (entity->positionSystemLink.IsLinked()) {
            entity->positionSystemLink.Unlink();
        }
        //Free memory used by the entity
        FreeBitSet(entity->componentDescriptor);
        delete(entity);
    }
    entityManager->entities.clear();
    entityManager->reclaimedIds.clear();

    delete(entityManager);
}

Entity CreateEntity(EntityManager entityManager) {
    Entity newEntity = new entity();
    newEntity->componentDescriptor = CreateBitSet(NUM_COMPONENT_TYPES);
    newEntity->id = GetNextAvailableEntityId(entityManager);

    entityManager->entities[newEntity->id] = newEntity;

    return newEntity;
}

void FreeEntity(EntityManager entityManager, Entity entity) {
    //Remove entity from all intrusive lists
    if (entity->renderSystemLink.IsLinked()) {
        entity->renderSystemLink.Unlink();
    }
    if (entity->cameraSystemLink.IsLinked()) {
        entity->cameraSystemLink.Unlink();
    }
    if (entity->animationSystemLink.IsLinked()) {
        entity->animationSystemLink.Unlink();
    }
    if (entity->positionSystemLink.IsLinked()) {
        entity->positionSystemLink.Unlink();
    }
    //Remove the entity from the entity managers registry
    entityManager->entities[entity->id] = NULL;
    entityManager->entityCount--;

    //Add the entities ID back to the entity managers pool
    entityManager->reclaimedIds[entityManager->reclaimedIdCount++] = entity->id;

    //Free memory used by the entity
    FreeBitSet(entity->componentDescriptor);
    delete(entity);
}
