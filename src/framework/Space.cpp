#include "Space.h"
#include "Component.h"
#include "Logger.h"

typedef std::vector<GameSystem>::iterator GameSystemItr;

space::space(ResourceManager resourceManager, Renderer renderer) :
    playerTemplate("entity/Player.entity", resourceManager),
    combatDummyTemplate("entity/CombatDummy.entity", resourceManager),
    componentManager(),
    box2dWorld(b2Vec2(0.0f, -10.0f)),
    box2dDebugRenderer(renderer),
    processing(false),
    drawCollisionDebugData(false){

    systems.push_back(CreateSystem(CAMERA_SYSTEM, renderer));
    systems.push_back(CreateSystem(ANIMATION_SYSTEM));
    systems.push_back(CreateSystem(RENDER_SYSTEM, renderer));

    this->tileEngine = CreateTileEngine(resourceManager, &box2dWorld, "map/City.tmx");
    this->entityManager = CreateEntityManager();
    this->player = CreateEntityFromTemplate(playerTemplate);

    AddEntity(this->player);
    AddEntity(CreateEntityFromTemplate(combatDummyTemplate));

    this->box2dWorld.SetAllowSleeping(false);
    this->box2dWorld.SetGravity(b2Vec2(0.0f, 0.0f));

    //Listen for contacts and handle appropriately
    this->box2dWorld.SetContactListener(this);

    //Allow drawing of collision bounding boxes for debug purposes.
    this->box2dWorld.SetDebugDraw( &box2dDebugRenderer );
    box2dDebugRenderer.SetFlags( b2Draw::e_shapeBit );
}

space::~space(){

}

void space::Process(Uint32 interval) {
    static float32 timeStep = 1/20.0;      //the length of time passed to simulate (seconds)
    static int32 velocityIterations = 8;   //how strongly to correct velocity
    static int32 positionIterations = 3;   //how strongly to correct position

    this->box2dWorld.Step( timeStep, velocityIterations, positionIterations);

    for(GameSystemItr itr = this->systems.begin(); itr != this->systems.end(); ++itr) {
        (*itr)->Process(this, interval);
    }
}

void space::RenderDebugCollisionData() {
    if (drawCollisionDebugData) {
        this->box2dWorld.DrawDebugData();
    }
}

void space::AddEntity(Entity entity) {
    for(GameSystemItr itr = this->systems.begin(); itr != this->systems.end(); ++itr) {
        unsigned int compareResult = CompareBitSets(entity->componentDescriptor, (*itr)->componentDescriptor);

        //For now all of the components listed by the system have to be fulfilled.
        if (compareResult == IntValueOfBitSet((*itr)->componentDescriptor)) {
            AddEntityToSystem((*itr), entity);
        }
    }
}

Entity space::CreateEntityFromTemplate(entityTemplate entityTemplate) {
    Entity entity = CreateEntity(this->entityManager);

    if (GetBit(entityTemplate.componentDescriptor, ANIMATION)) {
        AnimationComponent ac = this->componentManager.AddComponentToEntity<animationComponent>(entity);
        ac->animationMap = entityTemplate.animationComponent.animationMap;
    }

    if (GetBit(entityTemplate.componentDescriptor, PHYSICS)) {
        PhysicsComponent pc = this->componentManager.AddComponentToEntity<physicsComponent>(entity);
        pc->bodyDef = entityTemplate.physicsComponent.bodyDef;
        pc->collisionShape = entityTemplate.physicsComponent.collisionShape;
        pc->collisionFixtureDef.shape = &(pc->collisionShape);
        pc->collisionFixtureDef.filter.categoryBits = entityTemplate.physicsComponent.categoryBits;
        pc->collisionFixtureDef.filter.maskBits - entityTemplate.physicsComponent.maskBits;

        pc->body = box2dWorld.CreateBody(&(pc->bodyDef));
        pc->body->CreateFixture(&(pc->collisionFixtureDef));
        pc->body->SetUserData(entity);
    }

    if (GetBit(entityTemplate.componentDescriptor, CAMERA)) {
        CameraComponent cc = this->componentManager.AddComponentToEntity<cameraComponent>(entity);
        cc->lookAt = entityTemplate.cameraComponent.lookAt;
        cc->viewBounds = entityTemplate.cameraComponent.viewBounds;
    }

    if (GetBit(entityTemplate.componentDescriptor, SPRITE)) {
        SpriteComponent spriteComp = this->componentManager.AddComponentToEntity<spriteComponent>(entity);
        spriteComp->activeState = entityTemplate.spriteComponent.activeState;
        spriteComp->feetMap = entityTemplate.spriteComponent.feetMap;
        spriteComp->headMap = entityTemplate.spriteComponent.headMap;
        spriteComp->legsMap = entityTemplate.spriteComponent.legsMap;
        spriteComp->spriteMap = entityTemplate.spriteComponent.spriteMap;
        spriteComp->torsoMap = entityTemplate.spriteComponent.torsoMap;
        spriteComp->weaponMap = entityTemplate.spriteComponent.weaponMap;
    }

    if (GetBit(entityTemplate.componentDescriptor, STATUS)) {
        StatusComponent sc = this->componentManager.AddComponentToEntity<statusComponent>(entity);
        sc->heading = entityTemplate.statusComponent.heading;
        sc->state = entityTemplate.statusComponent.state;
    }

    return entity;
}

void space::BeginContact(b2Contact* contact) {
    Entity entityA= NULL;
    Entity entityB = NULL;
    Weapon weapon = NULL;

    //One entity should be (weapon) - this is temporary

    void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();

    if (PLAYER_WEAPON == contact->GetFixtureA()->GetFilterData().categoryBits) {
        weapon = static_cast<Weapon>( bodyUserData);
    } else if (PLAYER == contact->GetFixtureA()->GetFilterData().categoryBits
               || MOB == contact->GetFixtureA()->GetFilterData().categoryBits ) {
        entityA = static_cast<Entity>( bodyUserData );
    } else if (ENVIRONMENT == contact->GetFixtureA()->GetFilterData().categoryBits ) {
        //Nothing, environment is not presently destructible
    } else {
        assert(0);
    }

    bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();

    if (PLAYER_WEAPON == contact->GetFixtureB()->GetFilterData().categoryBits) {
        weapon = static_cast<Weapon>( bodyUserData);
    } else if (PLAYER == contact->GetFixtureB()->GetFilterData().categoryBits
               || MOB == contact->GetFixtureB()->GetFilterData().categoryBits ) {
        entityB = static_cast<Entity>( bodyUserData );
    } else if (ENVIRONMENT == contact->GetFixtureB()->GetFilterData().categoryBits ) {
        //Nothing, environment is not presently destructible
    } else {
        assert(0);
    }

    if ( (NULL != weapon) && (NULL != entityA) ) {
        StatusComponent statusComp = this->componentManager.GetComponentForEntity<statusComponent>(entityA);
        statusComp->state = HIT;
    } else if ((NULL != weapon) && (NULL != entityB) ) {
        StatusComponent statusComp = this->componentManager.GetComponentForEntity<statusComponent>(entityB);
        statusComp->state = HIT;
    }
}

void space::EndContact(b2Contact* contact) {
//    Weapon weaponA = NULL;
//    Weapon weaponB = NULL;

//    //One entity should be (weapon) - this is temporary

//    void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
//    if (PLAYER_WEAPON == contact->GetFixtureA()->GetFilterData().categoryBits) {
//        weaponA = static_cast<Weapon>( bodyUserData);
//    } else {
//        bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
//        if (PLAYER_WEAPON == contact->GetFixtureB()->GetFilterData().categoryBits) {
//            weaponB = static_cast<Weapon>( bodyUserData);
//        }
//    }

//    bool stillActive = false;
//    if (NULL != weaponA) {
//         stillActive = contact->GetFixtureA()->GetBody()->IsActive();
//    } else if (NULL != weaponB) {
//         stillActive = contact->GetFixtureB()->GetBody()->IsActive();
//    }

//    LOGI("Weapon stil active %s", (stillActive) ? "yes" : "no" );
}
