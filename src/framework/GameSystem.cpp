#include <cassert>
#include <SDL2/SDL.h>

#include "GameSystem.h"
#include "Component.h"
#include "RenderSystem.h"
#include "CameraSystem.h"
#include "AnimationSystem.h"
#include "PositionSystem.h"

GameSystem CreateSystem(SystemType systemType, ...) {
    GameSystem obj = NULL;

    va_list initValues;

    va_start(initValues, systemType);

    Renderer renderer = NULL;

    switch (systemType) {
    case RENDER_SYSTEM:
        renderer = va_arg(initValues, Renderer);
        obj = new renderSystem(renderer);
        obj->componentDescriptor = CreateBitSet(NUM_COMPONENT_TYPES);
        //Render system is interested in all entities with position and sprite components,
        //optionally cares about the camera component
        SetBit(obj->componentDescriptor, SPRITE);
        SetBit(obj->componentDescriptor, PHYSICS);
        obj->type = RENDER_SYSTEM;
        break;
    case CAMERA_SYSTEM:
        renderer = va_arg(initValues, Renderer);
        obj = new cameraSystem(renderer);
        obj->componentDescriptor = CreateBitSet(NUM_COMPONENT_TYPES);
        //Camera system is interested in any entity with a camera and position component
        SetBit(obj->componentDescriptor, PHYSICS);
        SetBit(obj->componentDescriptor, CAMERA);
        obj->type = CAMERA_SYSTEM;
        break;
    case ANIMATION_SYSTEM: {
        obj = new animationSystem();
        obj->componentDescriptor = CreateBitSet(NUM_COMPONENT_TYPES);
        SetBit(obj->componentDescriptor, SPRITE);
        SetBit(obj->componentDescriptor, STATUS);
        SetBit(obj->componentDescriptor, ANIMATION);
        obj->type = ANIMATION_SYSTEM;
        break;
    }
    default:
        assert(0);
    }

    va_end(initValues);

    assert(NULL != obj);

    return obj;
}

void FreeSystem(GameSystem system) {
    FreeBitSet(system->componentDescriptor);
    delete(system);
}

void AddEntityToSystem(GameSystem system, Entity entity) {
    system->AddEntity(entity);
}

void RemoveEntityFromSystem(GameSystem system, Entity entity) {
    system->RemoveEntity(entity);
}
