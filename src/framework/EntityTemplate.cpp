#include <exception>
#include <string>
#include <cstdlib>
#include <map>

#include "EntityTemplate.h"
#include "Logger.h"

#include "tinyxml.h"

entityTemplate::entityTemplate(std::string entityXmlFile,
                               ResourceManager resourceManager)
    : entityXmlFile(entityXmlFile) {
    this->componentDescriptor = CreateBitSet(NUM_COMPONENT_TYPES);
    ClearAll(this->componentDescriptor);
    Initialize(resourceManager);
}

entityTemplate::~entityTemplate() {

}

void entityTemplate::Initialize(ResourceManager resourceManager) {
    ResourceHandle entityResourceHandle = GetResource(resourceManager, entityXmlFile, ENTITY);
    TiXmlHandle doc(&(entityResourceHandle->entityTemplateResource->xml));
    TiXmlElement* root = doc.FirstChildElement().ToElement();
    if(NULL == root){
        assert(0);
    }

    for(TiXmlElement* elem = root->FirstChildElement(); elem != NULL; elem = elem->NextSiblingElement()) {
        std::string elemName = elem->Value();

        if ("animationComponent" == elemName) {
            SetBit(this->componentDescriptor, ANIMATION);
            for(TiXmlElement* animElem = elem->FirstChildElement(); animElem != NULL; animElem = animElem->NextSiblingElement()) {
                std::string animElemName = animElem->Value();
                if ("animation" != animElemName) {
                    assert(0);
                }

                EntityState entityState = IDLE;
                unsigned int interval = 0;

                const char* typeAttr = NULL;
                typeAttr = animElem->Attribute("state");
                std::string typeAttrName = typeAttr;
                if (NULL == typeAttr) {
                    assert(0);
                } else {
                    if ("walk" == typeAttrName) {
                        entityState = WALK;
                    } else if ("idle" == typeAttrName) {
                        entityState = IDLE;
                    } else if ("attack" == typeAttrName) {
                        entityState = ATTACK;
                    } else if ("hit" == typeAttrName) {
                        entityState = HIT;
                    } else {
                        assert(0);
                    }
                }

                typeAttr = animElem->Attribute("interval");
                if (NULL == typeAttr) {
                    assert(0);
                } else {
                    interval = atoi(typeAttr);
                }

                animationMetadata am;
                am.currentAnimationPointer = 0;
                am.interval = interval;
                am.intervalAccumulator = 0;

                for(TiXmlElement* animChildElem = animElem->FirstChildElement(); animChildElem != NULL; animChildElem = animChildElem->NextSiblingElement()) {
                    std::string animChildElemName = animChildElem->Value();
                    if ("frame" == animChildElemName) {
                        unsigned int x = 0;
                        unsigned int y = 0;
                        typeAttr = animChildElem->Attribute("x");
                        if (NULL == typeAttr) {
                            assert(0);
                        } else {
                            x = atoi(typeAttr);
                        }
                        typeAttr = animChildElem->Attribute("y");
                        if (NULL == typeAttr) {
                            assert(0);
                        } else {
                            y = atoi(typeAttr);
                        }
                        SDL_Point frameOffset;
                        frameOffset.x = x;
                        frameOffset.y = y;

                        am.animations.push_back(frameOffset);
                    } else if ("offset" == animChildElemName) {
                        typeAttr = animChildElem->Attribute("direction");
                        if (NULL == typeAttr) {
                            assert(0);
                        } else {
                            std::string direction = typeAttr;
                            unsigned int x = atoi(animChildElem->Attribute("x"));
                            unsigned int y = atoi(animChildElem->Attribute("y"));

                            if ("west" == direction) {
                                am.westFacingOffset.x = x;
                                am.westFacingOffset.y = y;
                            } else if ("east" == direction) {
                                am.eastFacingOffset.x = x;
                                am.eastFacingOffset.y = y;
                            } else if ("south" == direction) {
                                am.southFacingOffset.x = x;
                                am.southFacingOffset.y = y;
                            } else if ("north" == direction) {
                                am.northFacingOffset.x = x;
                                am.northFacingOffset.y = y;
                            }
                        }
                    } else {
                        assert(0);
                    }
                }
                this->animationComponent.animationMap[entityState] = am;
            }
        } else if ("spriteComponent" == elemName) {
            SetBit(this->componentDescriptor, SPRITE);
            std::string spriteActiveState = elem->Attribute("activeState");
            if ("idle" == spriteActiveState) {
                this->spriteComponent.activeState = IDLE;
            } else if ("walk" == spriteActiveState) {
                this->spriteComponent.activeState = WALK;
            } else if ("attack" == spriteActiveState) {
                this->spriteComponent.activeState = ATTACK;
            } else {
                assert(0);
            }

            for(TiXmlElement* spriteBodyPartElem = elem->FirstChildElement(); spriteBodyPartElem != NULL; spriteBodyPartElem = spriteBodyPartElem->NextSiblingElement()) {
                std::string spriteBodyPartElemName = spriteBodyPartElem->Value();
                std::map<EntityState, spriteMetadata>* map = NULL;

                if ("head"  == spriteBodyPartElemName) {
                    map = &(this->spriteComponent.headMap);
                } else if ("sprite"  == spriteBodyPartElemName) {
                    map = &(this->spriteComponent.spriteMap);
                } else if ("torso"  == spriteBodyPartElemName) {
                    map = &(this->spriteComponent.torsoMap);
                } else if ("legs"  == spriteBodyPartElemName) {
                    map = &(this->spriteComponent.legsMap);
                } else if ("feet"  == spriteBodyPartElemName) {
                    map = &(this->spriteComponent.feetMap);
                } else if ("weapon"  == spriteBodyPartElemName) {
                    map = &(this->spriteComponent.weaponMap);
                } else {
                    assert(0);
                }

                spriteMetadata sm;
                for(TiXmlElement* spriteElem = spriteBodyPartElem->FirstChildElement(); spriteElem != NULL; spriteElem = spriteElem->NextSiblingElement()) {
                    sm.height = atoi(spriteElem->Attribute("height"));
                    sm.width = atoi(spriteElem->Attribute("width"));
                    sm.spriteName = spriteElem->Attribute("name");
                    if (!sm.spriteName.empty()) {
                        sm.textureResource  = GetResource(resourceManager, sm.spriteName, TEXTURE);
                    }

                    SDL_Rect sourceRect;
                    sourceRect.h = sm.height;
                    sourceRect.w = sm.width;
                    sourceRect.x = atoi(spriteElem->Attribute("sourceX"));
                    sourceRect.y = atoi(spriteElem->Attribute("sourceY"));

                    sm.sourceRect = sourceRect;
                    std::string state = spriteElem->Attribute("entityState");
                    if ("idle" == state) {
                        (*map)[IDLE] = sm;
                    } else if ("walk" == state) {
                        (*map)[WALK] = sm;
                    } else if ("attack" == state) {
                        (*map)[ATTACK] = sm;
                    }  else if ("hit" == state) {
                        (*map)[HIT] = sm;
                    } else {
                        assert(0);
                    }
                }
            }
        } else if ("cameraComponent" == elemName) {
            SetBit(this->componentDescriptor, CAMERA);
            SDL_Point lookAt;
            lookAt.x = atoi(elem->Attribute("lookAtX"));
            lookAt.y = atoi(elem->Attribute("lookAtY"));

            SDL_Rect viewBounds;
            viewBounds.x = atoi(elem->Attribute("viewBoundsUpperLeftX"));
            viewBounds.y = atoi(elem->Attribute("viewBoundsUpperLeftY"));
            viewBounds.w = atoi(elem->Attribute("viewBoundsWidth"));
            viewBounds.h = atoi(elem->Attribute("viewBoundsHeight"));

            this->cameraComponent.lookAt = lookAt;
            this->cameraComponent.viewBounds = viewBounds;
        } else if ("physicsComponent" == elemName) {
            SetBit(this->componentDescriptor, PHYSICS);
            SDL_Point position;
            position.x = atoi(elem->Attribute("x"));
            position.y = atoi(elem->Attribute("y"));

            int angle = atoi(elem->Attribute("angle"));
            int collisionRadius = atoi(elem->Attribute("collisionRadius"));
            int collisionX = atoi(elem->Attribute("collisionX"));
            int collisionY = atoi(elem->Attribute("collisionY"));

            this->physicsComponent.collisionShape.m_p.Set(collisionX, collisionY); //position, relative to body position
            this->physicsComponent.collisionShape.m_radius = collisionRadius; //radius

            std::string bodyType = elem->Attribute("type");

            if ("dynamic" == bodyType) {
                this->physicsComponent.bodyDef.type = b2_dynamicBody;
            } else if ("static" == bodyType) {
                this->physicsComponent.bodyDef.type = b2_staticBody;
            } else {
                assert(0);
            }

            std::string category = elem->Attribute("category");

            if ("player" == category) {
                this->physicsComponent.categoryBits = PLAYER;
            } else if ("mob" == category) {
                this->physicsComponent.categoryBits = MOB;
            } else {
                assert(0);
            }

            std::string mask = elem->Attribute("mask");
            char* masks = strtok(const_cast<char*>(mask.c_str()), ",");

            this->physicsComponent.maskBits = 0x0000;

            while (NULL != masks) {
                if (0 == strcmp(masks, "player")) {
                    this->physicsComponent.maskBits |= PLAYER;
                } else if (0 == strcmp(masks, "player_weapon")) {
                    this->physicsComponent.maskBits |= PLAYER_WEAPON;
                } else if (0 == strcmp(masks, "mob")) {
                    this->physicsComponent.maskBits |= MOB;
                } else if (0 == strcmp(masks, "mob_weapon")) {
                    this->physicsComponent.maskBits |= MOB_WEAPON;
                } else {
                    assert(0);
                }

                masks = strtok(NULL, ",");
            }

            this->physicsComponent.bodyDef.position.Set(position.x, position.y);
            this->physicsComponent.bodyDef.angle = angle;
            this->physicsComponent.bodyDef.linearVelocity.Set(0.0f, 0.0f);
        } else if ("statusComponent" == elemName) {
            SetBit(this->componentDescriptor, STATUS);
            std::string heading = elem->Attribute("heading");
            std::string state = elem->Attribute("entityState");

            if ("north" == heading) {
                this->statusComponent.heading = NORTH;
            } else if ("south" == heading) {
                this->statusComponent.heading = SOUTH;
            } else if ("west" == heading) {
                this->statusComponent.heading = WEST;
            } else if ("east" == heading) {
                this->statusComponent.heading = EAST;
            } else {
                assert(0);
            }

            if ("idle" == state) {
                this->statusComponent.state = IDLE;
            } else if ("walk" == state) {
                this->statusComponent.state = WALK;
            } else if ("attack" == state) {
                this->statusComponent.state = ATTACK;
            } else {
                assert(0);
            }
        } else {
            assert(0);
        }
    }
}
