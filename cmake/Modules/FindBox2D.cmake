# - Try to find Box2D
# Once done this will define
#  BOX2D_FOUND - System has Box2D
#  BOX2D_INCLUDE_DIRS - The Box2D  include directories
#  BOX2D_LIBRARIES - The libraries needed to use Box2D

find_path(BOX2D_INCLUDE_DIR Box2D/Box2D.h
	  PATH_SUFFIXES include)

find_library(BOX2D_LIBRARY NAMES BOX2D Box2D box2d Box2d)

set(BOX2D_LIBRARIES ${BOX2D_LIBRARY} )
set(BOX2D_INCLUDE_DIRS ${BOX2D_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set BOX2D_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Box2D DEFAULT_MSG
                                  BOX2D_LIBRARY BOX2D_INCLUDE_DIR)

mark_as_advanced(BOX2D_INCLUDE_DIR BOX2D_LIBRARY)
