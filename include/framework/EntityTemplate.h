/**
 * EntityFactory.h
 */
#ifndef ENTITY_FACTORY_HEADER_SEEN
#define ENTITY_FACTORY_HEADER_SEEN

#include "Component.h"
#include "Entity.h"
#include "ResourceManager.h"
#include "bitset.h"

#include <string>

class entityTemplate {
public:
    entityTemplate(std::string entityXmlFile, ResourceManager resourceManager);
    //Not intended for inheritance
    ~entityTemplate();

private:
    void Initialize(ResourceManager resourceManager);

public:
    BitSet componentDescriptor;
    struct spriteComponent spriteComponent;
    struct animationComponent animationComponent;
    struct statusComponent statusComponent;
    struct cameraComponent cameraComponent;
    struct physicsComponent physicsComponent;

private:
    std::string entityXmlFile;
};
#endif
