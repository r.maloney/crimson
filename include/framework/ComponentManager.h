/**
 * component_manager.h
 *
 * Component manager
 */
#ifndef COMPONENT_MANAGER_HEADER_SEEN
#define COMPONENT_MANAGER_HEADER_SEEN

#include "Component.h"
#include "Entity.h"
#include "ResourceManager.h"
#include "ComponentPool.h"
#include "EntityTemplate.h"

#include <cstdarg>
#include <map>
#include <vector>

struct compTagBase {
    unsigned int index;
    compTagBase(unsigned int index) : index(index) {}
};

struct animationCompTag : compTagBase { animationCompTag() : compTagBase(0) {} };
struct cameraCompTag : compTagBase{cameraCompTag() : compTagBase(1) {}};
struct physicsCompTag : compTagBase{physicsCompTag() : compTagBase(2) {}};
struct spriteCompTag : compTagBase{spriteCompTag() : compTagBase(3) {}};
struct statusCompTag : compTagBase{statusCompTag() : compTagBase(4) {}};

template < typename T > struct componentTag;
template < > struct componentTag<animationComponent> { typedef animationCompTag type; };
template < > struct componentTag<cameraComponent> { typedef cameraCompTag type; };
template < > struct componentTag<physicsComponent> { typedef physicsCompTag type; };
template < > struct componentTag<spriteComponent> { typedef spriteCompTag type; };
template < > struct componentTag<statusComponent> { typedef statusCompTag type; };

class compManager {
public:
    compManager(unsigned int startingPoolSize = 100, unsigned int maximumEntityCount = 100);
    //Not intended for inheritance
    ~compManager();

public:
    template <typename T>
    T* GetComponentForEntity(Entity entity){
        typedef typename componentTag<T>::type tag;

        return GetComponentForEntity<T>(entity, tag());
    }

    template <typename T>
    T* AddComponentToEntity(Entity entity) {
        typedef typename componentTag<T>::type tag;

        return AddComponentToEntity(entity, tag());
    }

private:
    template <typename T>
    T* GetComponentForEntity(Entity entity, compTagBase tag) {
        return (T*)this->entityComponentMap[entity->id * NUM_COMPONENT_TYPES + tag.index];
    }

    AnimationComponent AddComponentToEntity(Entity entity, animationCompTag tag);
    CameraComponent AddComponentToEntity(Entity entity, cameraCompTag tag);
    PhysicsComponent AddComponentToEntity(Entity entity, physicsCompTag tag);
    SpriteComponent AddComponentToEntity(Entity entity, spriteCompTag tag);
    StatusComponent AddComponentToEntity(Entity entity, statusCompTag tag);

private:
    //Row Major order
    std::vector<ComponentBase> entityComponentMap;
    componentPool<animationComponent> animationComponentPool;
    componentPool<cameraComponent> cameraComponentPool;
    componentPool<physicsComponent> physicsComponentPool;
    componentPool<spriteComponent> spriteComponentPool;
    componentPool<statusComponent> statusComponentPool;
};

#endif
