/**
 * Space.h
 *
 */
#ifndef SPACE_HEADER_SEEN
#define SPACE_HEADER_SEEN

#include "ComponentManager.h"
#include "EntityManager.h"
#include "EntityTemplate.h"
#include "ResourceManager.h"
#include "Entity.h"
#include "GameSystem.h"
#include "Renderer.h"
#include "TileEngine.h"
#include "BoundingBoxRenderer.h"

#include <Box2D/Box2D.h>
#include <vector>

typedef class space* Space;

class space : public b2ContactListener{
public:
    space(ResourceManager resourceManager, Renderer renderer);
    virtual ~space();

public:
    /**
      *From b2ContactListener
      */
    virtual void BeginContact(b2Contact* contact);
    virtual void EndContact(b2Contact* contact);

public:
    void Process(Uint32 interval);
    void RenderDebugCollisionData();
    void AddEntity(Entity entity);

private:
    Entity CreateEntityFromTemplate(entityTemplate entityTemplate);

public:
    TileEngine tileEngine;
    compManager componentManager;
    bool processing;
    bool drawCollisionDebugData;
    Entity player;
    //Hacky - rework once basic weapon collision is in place.
    b2World box2dWorld;

private:
    EntityManager entityManager;
    entityTemplate playerTemplate;
    entityTemplate combatDummyTemplate;
    std::vector<GameSystem> systems;
    boundingBoxRenderer box2dDebugRenderer;
};

#endif
