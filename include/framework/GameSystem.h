/**
 * system.h
 */
#ifndef GAMESYSTEM_HEADER_SEEN
#define GAMESYSTEM_HEADER_SEEN

#include "bitset.h"
#include "Entity.h"
#include "List.h"

#include <cstdarg>
#include <SDL2/SDL.h>

//Forward Declarations
typedef struct world* World;
typedef class space* Space;

typedef enum {
    RENDER_SYSTEM,
    CAMERA_SYSTEM,
    ANIMATION_SYSTEM,
    NUM_SYSTEM_TYPES
} SystemType;

typedef struct gameSystem* GameSystem;

class gameSystem {
public:
    gameSystem() {

    }
    virtual ~gameSystem() {

    }

public:
    LIST_LINK(gameSystem) worldLink;
    SystemType type;
    ComponentDescriptor componentDescriptor;

public:
    virtual void Process(Space space, Uint32 interval ) = 0;
    virtual void AddEntity(Entity entity) = 0;
    virtual void RemoveEntity(Entity entity) = 0;
};

GameSystem CreateSystem(SystemType systemType, ...);
void FreeSystem(GameSystem system);

void AddEntityToSystem(GameSystem system, Entity entity);
void RemoveEntityFromSystem(GameSystem system, Entity entity);

#endif
