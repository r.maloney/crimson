/**
 * ComponentPool.h
 */
#ifndef COMPONENTPOOL_HEADER_SEEN
#define COMPONENTPOOL_HEADER_SEEN

#include <vector>
#include <stack>

template<class T>
class componentPool {
    public:
        componentPool(int capacity) {
            this->components.resize(capacity);
            for (unsigned int i = 0; i < capacity; ++i) {
                this->availableComponents.push(i);
                this->components[i].poolId = i;
            }
        }

        //Intentionally not virtual, this class is not intended for extension.
        ~componentPool() {
            this->components.clear();
        }

        T* AllocateComponent() {
            if (0 == this->availableComponents.size()) {
                std::exception e;
                throw e;
            }

            unsigned int nextComponent = this->availableComponents.top();

            this->availableComponents.pop();

            return &(this->components[nextComponent]);
        }

        void FreeComponent(T& component) {
            unsigned int i = component.poolId;
            this->availableComponents.push(i);
            //TODO: clear
        }

    private:
        std::vector<T> components;
        std::stack<unsigned int> availableComponents;
};

#endif
