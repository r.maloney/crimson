/**
 * TileEngine.h
 */
#ifndef TILEENGINE_HEADER_SEEN
#define TILEENGINE_HEADER_SEEN

#include <SDL2/SDL.h>
#include <string>
#include <Box2D/Box2D.h>

#include "Renderer.h"
#include "ResourceManager.h"

class tileEngine;
typedef tileEngine* TileEngine;

class tileEngine {
    public:
        tileEngine(unsigned int mapWidth,
                   unsigned int mapHeight,
                   unsigned int tileWidth,
                   unsigned int tileHeight,
                   unsigned int layers,
                   ResourceHandle cursorTexture,
                   b2World* box2dWorld = NULL);
        ~tileEngine();

    public:
        void RegisterTile(ResourceManager resourceManager, unsigned int id, std::string filename,int x, int y);
        void SetTile(unsigned int id, int x, int y, int layer);
        unsigned int GetTile(int x, int y, int layer);
        void RenderBackground(Renderer renderer);
        void RenderForeground(Renderer renderer);
        void RenderGrid(Renderer renderer);
        void RecomputeFieldOfView(int& x, int& y, int fovRadius);
        bool IsTraversable(SDL_Rect playerBoundingBox);
        void ToggleGridDraw();
        unsigned int GetTileWidth();
        unsigned int GetTileHeight();
        void UpdateTerrain(int x, int y, bool traversable = true, bool transparent = true);
        void AddObject(SDL_Rect objectBoundingBox);
        SDL_Rect const* GetMapBoundaries();

    private:
        void CalculateVisibleMap(Renderer renderer, int& xMin, int& xMax, int& yMin, int& yMax);

    public:
        SDL_Point mouseLocation;
        SDL_Point selectedTile;

    private:
        bool drawGrid;
        b2World* box2dWorld;
        //Declared internally
        void* collisionData;
        void* data;
};

TileEngine CreateTileEngine(ResourceManager resourceManager,
                            unsigned int mapWidth,
                            unsigned int mapHeight,
                            unsigned int tileWidth,
                            unsigned int tileHeight,
                            unsigned int layers);

TileEngine CreateTileEngine(ResourceManager resourceManager,
                            b2World* box2dWorld,
                            std::string tmxFileName);

#endif
