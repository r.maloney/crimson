/**
 * EventMessage.h
 */
#ifndef EVENTMESSAGE_HEADER_SEEN
#define EVENTMESSAGE_HEADER_SEEN

#include <string>

typedef enum {
    SYSTEM_MSG,
    MOVE_MSG,
    ATTACK_MSG,
    NUM_MESSAGE_TYPES
} MessageType;

struct eventMessage {
    MessageType type;
    std::string message;
};

#endif
