#ifndef CONSOLEBUFFER_H
#define CONSOLEBUFFER_H

#include "EventMessage.h"

#include <Rocket/Controls/DataSource.h>
#include <Rocket/Core/Types.h>
#include <deque>

const int BUFFER_SIZE = 7;

class ConsoleBuffer : public Rocket::Controls::DataSource {
public:
    static void Initialise();
    static void Shutdown();
    static void AddMessage(eventMessage eventMessage);

private:
    static ConsoleBuffer* instance;

public:
    void GetRow(Rocket::Core::StringList& row, const Rocket::Core::String& table, int row_index, const Rocket::Core::StringList& columns);
    int GetNumRows(const Rocket::Core::String& table);

private:
    ConsoleBuffer();
    ~ConsoleBuffer();

private:
    void InternalAddMessage(eventMessage eventMessage);

private:
    std::deque<eventMessage> messages;
    Rocket::Core::Colourb systemMsgColor;
    Rocket::Core::String systemMsgColorString;
    Rocket::Core::Colourb moveMsgColor;
    Rocket::Core::String moveMsgColorString;
    Rocket::Core::Colourb attackMsgColor;
    Rocket::Core::String attackMsgColorString;
};

#endif
