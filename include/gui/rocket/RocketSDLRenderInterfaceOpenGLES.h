#ifndef SDLROCKETGLUEGLES_H
#define SDLROCKETGLUEGLES_H
#include "RocketGlue.h"

#if SDL_VIDEO_RENDER_OGL_ES

class RocketSDLRenderInterfaceOpenGLES : public RocketSDLRenderInterface {
public:
    RocketSDLRenderInterfaceOpenGLES(SDL_Renderer *r, SDL_Window *w);

    /// Called by Rocket when it wants to render geometry that it does not wish to optimise.
    virtual void RenderGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rocket::Core::TextureHandle texture, const Rocket::Core::Vector2f& translation);

    /// Called by Rocket when it wants to enable or disable scissoring to clip content.
    virtual void EnableScissorRegion(bool enable);

    /// Called by Rocket when it wants to change the scissor region.
    virtual void SetScissorRegion(int x, int y, int width, int height);
};

#endif //SDL_VIDEO_RENDER_OGL_ES
#endif
