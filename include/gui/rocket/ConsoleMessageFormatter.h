#ifndef CONSOLEMESSAGEFORMATTER_H
#define CONSOLEMESSAGEFORMATTER_H

#include <Rocket/Controls/DataFormatter.h>

class ConsoleMessageFormatter : public Rocket::Controls::DataFormatter {
public:
    ConsoleMessageFormatter();
    ~ConsoleMessageFormatter();

    void FormatData(Rocket::Core::String& formatted_data, const Rocket::Core::StringList& raw_data);
};

#endif
