#ifndef SDLROCKETGLUEGLES2_H
#define SDLROCKETGLUEGLES2_H
#include "RocketGlue.h"

#if SDL_VIDEO_RENDER_OGL_ES2
#include <string>
#include <SDL2/SDL_opengles2.h>

class RocketSDLRenderInterfaceOpenGLES2 : public RocketSDLRenderInterface {
private:
    //std::string RocketGlueFragmentShader, RocketGlueVertexShader;

protected:
    GLuint program_texture_id, program_color_id, fragment_texture_shader_id, fragment_color_shader_id, vertex_shader_id;
    GLuint u_texture, u_texture_projection, u_texture_translation, u_color_projection, u_color_translation;

public:
    RocketSDLRenderInterfaceOpenGLES2(SDL_Renderer *r, SDL_Window *w);

    /// Called by Rocket when it wants to render geometry that it does not wish to optimise.
    virtual void RenderGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rocket::Core::TextureHandle texture, const Rocket::Core::Vector2f& translation);

    /// Called by Rocket when it wants to enable or disable scissoring to clip content.
    virtual void EnableScissorRegion(bool enable);

    /// Called by Rocket when it wants to change the scissor region.
    virtual void SetScissorRegion(int x, int y, int width, int height);
};

#endif // SDL_VIDEO_RENDER_OGL_ES2
#endif // SDLROCKETGLUEGLES2_H
