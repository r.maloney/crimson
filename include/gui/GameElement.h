#ifndef ROCKETGAMEELEMENT_H
#define ROCKETGAMEELEMENT_H

#include "Space.h"
#include "Entity.h"
#include "TileEngine.h"
#include "Component.h"

#include <Rocket/Core/Element.h>
#include <Rocket/Core/EventListener.h>
#include <Box2D/Box2D.h>

class GameElement : public Rocket::Core::Element, public Rocket::Core::EventListener {
public:
    GameElement(const Rocket::Core::String& tag);
    virtual ~GameElement();

    void ProcessEvent(Rocket::Core::Event& event);

    void OnChildAdd(Rocket::Core::Element* element);

    void SetSpace(Space space);

protected:
    virtual void OnUpdate();
    virtual void OnRender();

private:
    Entity player;
    Space space;
    StatusComponent statusComp;
    PhysicsComponent physicsComp;
    AnimationComponent animationComp;
    TileEngine tileEngine;
    SDL_Joystick* joystick;

    //A bit hacky but good enough until I sort out equipping/unequipping weapons.
    b2Body* weaponBody;
    float amountToIncrementEachRound;
};

#endif
