/**
 * Renderer.h
 */
#ifndef RENDERER_HEADER_SEEN
#define RENDERER_HEADER_SEEN

#include <SDL2/SDL.h>

typedef struct renderer* Renderer;

struct renderer {
    SDL_Renderer* sdlRenderer;
    SDL_Rect view;

    void Clear(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
    void Render(SDL_Texture* texture, SDL_Rect& sourceRect, SDL_Point& worldCoords);
    void Render(SDL_Color* color, SDL_Rect& destRect, SDL_Point& worldCoords);
    void RenderLine(SDL_Color& color, SDL_Point& p1WorldCoords, SDL_Point& p2WorldCoords);
    void RenderPoint(SDL_Color& color, SDL_Point& worldCoords);
    void RenderCircle(SDL_Color& color, SDL_Point& center, int radius);
    void Flip();
    SDL_Point ConvertScreenCoordsToWorld(SDL_Point& screenCoords, SDL_Rect& destRect);

private:
    //Loads the destRect with the screen coords if inside screen. Returns
    //true if worldCoords are in screen, false otherwise
    bool GetScreenCoordinates(SDL_Rect* sourceRect, SDL_Rect& destRect, SDL_Point& worldCoords);
};

Renderer CreateRenderer(SDL_Window* window);

void FreeRenderer(Renderer renderer);

#endif
