/**
 * resource_manager.h
 */
#ifndef RESOURCEMANAGER_HEADER_SEEN
#define RESOURCEMANAGER_HEADER_SEEN

#include <string>
#include <SDL2/SDL.h>

#include "TmxMap.h"
#include "tinyxml.h"

typedef enum {
    TEXTURE,
    TMX_MAP,
    ENTITY,
    NUM_RESOURCE_TYPES
} ResourceType;

typedef struct resourceManager* ResourceManager;

typedef struct textureResource* TextureResource;

struct textureResource {
    std::string name;
    SDL_Texture* texture;
};

typedef struct tmxMapResource* TmxMapResource;

struct tmxMapResource {
    std::string name;
    Tmx::Map map;
};

typedef struct entityTemplateResource* EntityTemplateResource;

struct entityTemplateResource {
    std::string name;
    TiXmlDocument xml;
};

typedef struct resourceHandle* ResourceHandle;

struct resourceHandle {
    ResourceType resourceType;

    union {
        TextureResource textureRes;
        TmxMapResource tmxMapResource;
        EntityTemplateResource entityTemplateResource;
    };
};

ResourceManager CreateResourceManager(SDL_Renderer* renderer, std::string assetBaseDir);

void FreeResourceManager(ResourceManager resourceManager);

void AddResource(ResourceManager resourceManager, std::string identifier, ResourceType resourceType);

ResourceHandle GetResource(ResourceManager resourceManager, std::string identifier, ResourceType resourceType);

#endif
