#ifndef ROCKETEVENTHANDLER_H
#define ROCKETEVENTHANDLER_H

#include <Rocket/Core/Event.h>
#include <Rocket/Core/String.h>

class EventHandler {
public:
    virtual ~EventHandler();
    virtual void ProcessEvent(Rocket::Core::Event& event, const Rocket::Core::String& value) = 0;
};


#endif
