#ifndef ROCKETEVENTMANAGER_H
#define ROCKETEVENTMANAGER_H

#include <Rocket/Core/Context.h>
#include <Rocket/Core/Event.h>

class EventHandler;

class EventManager {
public:
    static void Shutdown();

    static void RegisterEventHandler(const Rocket::Core::String& handler_name, EventHandler* handler);

    static void ProcessEvent(Rocket::Core::Event& event, const Rocket::Core::String& value);

    static bool LoadWindow(const Rocket::Core::String& window_name);

public:
    static Rocket::Core::Context* context;

private:
    EventManager();
    ~EventManager();
};


#endif
