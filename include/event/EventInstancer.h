#ifndef ROCKETEVENTINSTANCER_H
#define ROCKETEVENTINSTANCER_H

#include <Rocket/Core/EventListenerInstancer.h>

class EventInstancer : public Rocket::Core::EventListenerInstancer {
public:
    EventInstancer();

    virtual ~EventInstancer();

    virtual Rocket::Core::EventListener* InstanceEventListener(const Rocket::Core::String& value, Rocket::Core::Element* element);

    virtual void Release();
};

#endif
