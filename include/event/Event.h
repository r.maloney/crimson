#ifndef ROCKETEVENT_H
#define ROCKETEVENT_H

#include <Rocket/Core/EventListener.h>

class Event : public Rocket::Core::EventListener {
public:
    Event(const Rocket::Core::String& value);
    virtual ~Event();

    virtual void ProcessEvent(Rocket::Core::Event& event);

    virtual void OnDetach(Rocket::Core::Element* element);

private:
    Rocket::Core::String value;
};

#endif
