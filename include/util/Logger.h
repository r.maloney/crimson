#ifndef LIST_H
#define LIST_H

#ifdef __ANDROID__
    #include <android/log.h>
    #define LOG_TAG "crimson"
    #define LOGI(FORMAT_STR, ...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,FORMAT_STR,__VA_ARGS__)
    #define LOGE(FORMAT_STR, ...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,FORMAT_STR,__VA_ARGS__)
#else
    #include <logog/logog.hpp>
    #define LOGI(FORMAT_STR, ...)  INFO(FORMAT_STR,__VA_ARGS__)
    #define LOGE(FORMAT_STR, ...)  ERR(FORMAT_STR,__VA_ARGS__)
#endif

#endif
