/**
 * PositionSystem.h
 */
#ifndef POSITIONSYSTEM_HEADER_SEEN
#define POSITIONSYSTEM_HEADER_SEEN

#include "GameSystem.h"

class positionSystem : public gameSystem {
public:
    positionSystem();
    virtual ~positionSystem();

public:
    virtual void Process(Space space, Uint32 interval);
    virtual void AddEntity(Entity entity);
    virtual void RemoveEntity(Entity entity);

private:
    LIST_DECLARE(entity, positionSystemLink) entities;
    Uint32 intervalAccumulator;
};


#endif
