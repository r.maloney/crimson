/**
 * AnimationSystem.h
 */
#ifndef ANIMATIONSYSTEM_HEADER_SEEN
#define ANIMATIONSYSTEM_HEADER_SEEN

#include "GameSystem.h"

class animationSystem : public gameSystem {
public:
    animationSystem();
    virtual ~animationSystem();

public:
    virtual void Process(Space space, Uint32 interval);
    virtual void AddEntity(Entity entity);
    virtual void RemoveEntity(Entity entity);

private:
    LIST_DECLARE(entity, animationSystemLink) entities;
    Uint32 intervalAccumulator;
};

#endif
