/**
 * CameraSystem.h
 */
#ifndef CAMERASYSTEM_HEADER_SEEN
#define CAMERASYSTEM_HEADER_SEEN

#include "GameSystem.h"
#include "Renderer.h"

class cameraSystem : public gameSystem {
public:
    cameraSystem(Renderer renderer);
    virtual ~cameraSystem();

public:
    virtual void Process(Space space, Uint32 interval);
    virtual void AddEntity(Entity entity);
    virtual void RemoveEntity(Entity entity);

private:
    LIST_DECLARE(entity, cameraSystemLink) entities;
    Renderer renderer;
};

#endif
