/**
 * RenderSystem.h
 */
#ifndef RENDERSYSTEM_HEADER_SEEN
#define RENDERSYSTEM_HEADER_SEEN

#include "GameSystem.h"
#include "Renderer.h"

class renderSystem : public gameSystem {
public:
    renderSystem(Renderer renderer);
    virtual ~renderSystem();

public:
    virtual void Process(Space space, Uint32 interval);
    virtual void AddEntity(Entity entity);
    virtual void RemoveEntity(Entity entity);

private:
    LIST_DECLARE(entity, renderSystemLink) entities;
    Renderer renderer;
};

#endif
