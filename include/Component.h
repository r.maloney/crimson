/**
 * component.h
 *
 * Component management
 */

#ifndef COMPONENT_HEADER_SEEN
#define COMPONENT_HEADER_SEEN

#include "ResourceManager.h"

#include <vector>
#include <string>
#include <SDL2/SDL.h>
#include <Box2D/Box2D.h>

typedef enum {
   PLAYER = 0x0001,
   PLAYER_WEAPON = 0x0002,
   MOB = 0x0004,
   MOB_WEAPON = 0x0008,
   ENVIRONMENT = 0x0010
 } EntityCategory;

typedef enum {
    ANIMATION,
    CAMERA,
    PHYSICS,
    SPRITE,
    STATUS,
    NUM_COMPONENT_TYPES
} ComponentType;

typedef enum {
    CENTER,
    NORTH,
    SOUTH,
    EAST,
    WEST,
    NORTHEAST,
    NORTHWEST,
    SOUTHEAST,
    SOUTHWEST
} HeadingType;

typedef enum {
    IDLE,
    WALK,
    ATTACK,
    HIT
} EntityState;

//Temporary until I decide how to handle weapons
typedef struct weapon* Weapon;
struct weapon {

};

typedef struct componentBase* ComponentBase;
struct componentBase {
    unsigned int poolId;
};

struct spriteMetadata {
    ResourceHandle textureResource;
    std::string spriteName;
    SDL_Rect sourceRect;
    unsigned int width;
    unsigned int height;
};
const struct spriteMetadata DEFAULT_SPRITE_METADATA = {
    NULL,
    "",
    {0,0,0,0},
    0,0
};

typedef struct spriteComponent* SpriteComponent;
struct spriteComponent : public componentBase {
    std::map<EntityState, spriteMetadata> headMap;
    std::map<EntityState, spriteMetadata> torsoMap;
    std::map<EntityState, spriteMetadata> legsMap;
    std::map<EntityState, spriteMetadata> feetMap;
    std::map<EntityState, spriteMetadata> weaponMap;
    std::map<EntityState, spriteMetadata> spriteMap;
    EntityState activeState;
};

struct animationMetadata {
    int currentAnimationPointer;
    std::vector<SDL_Point> animations;

    SDL_Point westFacingOffset;
    SDL_Point eastFacingOffset;
    SDL_Point northFacingOffset;
    SDL_Point southFacingOffset;

    Uint32 intervalAccumulator;
    Uint32 interval;
};

typedef struct animationComponent* AnimationComponent;
struct animationComponent : public componentBase {
    std::map<EntityState, animationMetadata> animationMap;
};

typedef struct statusComponent* StatusComponent;
struct statusComponent : public componentBase {
    EntityState state;
    HeadingType heading;
};

typedef struct cameraComponent* CameraComponent;
struct cameraComponent : public componentBase {
    SDL_Rect viewBounds;
    SDL_Point lookAt;

    void SetLookAt(int x, int y) {
        this->lookAt.x = x;
        this->lookAt.y = y;

        this->viewBounds.x = x - (viewBounds.w / 2);
        this->viewBounds.y = y - (viewBounds.h / 2);
    }
};

typedef struct physicsComponent* PhysicsComponent;
struct physicsComponent : public componentBase {
    b2BodyDef bodyDef;
    b2Body* body;
    b2CircleShape collisionShape;
    b2FixtureDef collisionFixtureDef;
    uint16 maskBits;
    uint16 categoryBits;
};

#endif
