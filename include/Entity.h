/**
 * entity.h
 *
 * Methods for creating and manipulating entities
 */
#ifndef ENTITY_HEADER_SEEN
#define ENTITY_HEADER_SEEN

#include "bitset.h"
#include "List.h"

enum {
    MAX_ENTITY_NUM = 100
};

typedef struct entity* Entity;
typedef BitSet ComponentDescriptor;

struct entity {
    LIST_LINK(entity) renderSystemLink;
    LIST_LINK(entity) cameraSystemLink;
    LIST_LINK(entity) animationSystemLink;
    LIST_LINK(entity) positionSystemLink;
    ComponentDescriptor componentDescriptor;
    unsigned int id;
};

#endif
